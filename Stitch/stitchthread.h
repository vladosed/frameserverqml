#ifndef STITCHTHREAD_H
#define STITCHTHREAD_H

#include <QObject>
#include "Stitch.h"
#include <QStringList>
#include <QDebug>
#include <QVariant>
#include <QTime>
#include <QCoreApplication>
#include <QFile>

class StitchThread : public QObject
{
    Q_OBJECT
public:
    void setData(QStringList list, QString path, double coeff);
    explicit StitchThread(QObject *parent = nullptr);
    void calculate();

    void startLog(QString str);
    void log(QString str);
    void endLog(QString str);



signals:
    void progressChanged(QVariant value);
    void logChanged(QVariant str);

private:
    QStringList file_list;
    QString m_path;
    double m_coeff;
    QString inner_log = "";

public slots:
    //void stitchFinished();
};

#endif // STITCHTHREAD_H
