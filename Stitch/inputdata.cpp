#include "inputdata.h"

InputData::InputData(QObject *parent) : QObject(parent)
{
    connect(&thread, &StitchThread::progressChanged,
            this, &InputData::setProgress);
    connect(&thread, &StitchThread::logChanged,
            this, &InputData::addLog);

}

void InputData::parseFile(QString filename)
{
    this->file_list.clear();
    this->m_path = "";

    filename = filename.replace("file:///", "");
    qDebug() << filename;
    QFile inputFile(filename);
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);

        // resize coefficient
        this->m_coeff = in.readLine().toDouble();
        if (in.atEnd()){
            qDebug() << "End of file (haven't path)";
            return;
        }


        // path
        if (in.atEnd()){
            qDebug() << "End of file (haven't list of file)";
            return;
        }
        this->m_path = in.readLine().replace("\\", "/");


        // all files
        while (!in.atEnd())
        {
            QString line = in.readLine();
            this->file_list += line.split(" ", QString::SkipEmptyParts);
        }
        inputFile.close();
    }

    thread.setData(file_list, m_path, m_coeff);

    if (this->file_list.size()>0){
        setProgress(0);
        QFuture<void> future = QtConcurrent::run(&thread, &StitchThread::calculate);
        //process();
    }
    else
        qDebug() << "Needed 2 or more photos";
}

void InputData::writeVideo()
{
    if (this->m_videoRecording == false){
        this->m_videoRecording = true;
        QFuture<void> future = QtConcurrent::run(this, &InputData::writeVideoProcess);
    }
    else{
        this->m_videoRecording = false;
    }
    emit recordingChanged();

}

void InputData::writeVideoProcess()
{
    VideoCapture cap(0);
    QTime myTimer;
    myTimer.start();

    // Check if camera opened successfully
    if(!cap.isOpened())
    {
        cout << "Error opening video stream" << endl;
        return;
    }

    // Default resolution of the frame is obtained.The default resolution is system dependent.
    int frame_width = 1920;
    int frame_height = 1080;

    // Define the codec and create VideoWriter object.
    QString video_name;
    this->video_number++;
    video_name = QString("video/%1.avi").arg(this->video_number);
    qDebug() << video_name;
    VideoWriter video(video_name.toStdString(),VideoWriter::fourcc('M','J','P','G'),30, Size(frame_width,frame_height));

    while(1)
    {
        Mat frame;

        cap >> frame;

        if (frame.empty())
            break;

        video.write(frame);

        if (this->m_videoRecording == false)
        {
            break;
        }

        // Press  ESC on keyboard to  exit
    }

    cap.release();
    video.release();
}

void InputData::process()
{    
    for (int i = 1; i < this->file_list.size() - 1; i ++){
        QString first_image = this->m_path + "/" + this->file_list.at(i-1);
        QString second_image = this->m_path + "/" + this->file_list.at(i);
        Stitch stitch(first_image.toStdString(), second_image.toStdString(), this->m_coeff);
        stitch.calculate();
    }
}

void InputData::setProgress(QVariant value)
{
    this->m_stitchProgress = value;
    emit progressChanged();
}

void InputData::addLog(QVariant str)
{
    this->m_log.append(str.toString());
    emit logChanged();
}

