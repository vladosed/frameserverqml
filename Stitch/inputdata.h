#ifndef INPUTDATA_H
#define INPUTDATA_H

#include <QObject>
#include <QVariant>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QStringList>
#include <QCoreApplication>
#include <QFutureWatcher>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <opencv2/videoio.hpp>
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"

#include "stitchthread.h"

class InputData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant coef READ getCoeff NOTIFY coeffChanged)
    Q_PROPERTY(QVariant stitchProgress READ getProgress NOTIFY progressChanged)
    Q_PROPERTY(QVariant log READ getLog NOTIFY logChanged)
    Q_PROPERTY(QVariant v_recording READ getVRecording NOTIFY recordingChanged)

public:
    explicit InputData(QObject *parent = nullptr);
    Q_INVOKABLE void parseFile(QString filename);
    void writeVideoProcess();


private:
    QVariant getCoeff() {return QVariant::fromValue(this->m_coeff);}
    QVariant getProgress() {return this->m_stitchProgress;}
    QVariant getLog() {return QVariant::fromValue(this->m_log);}
    QVariant getVRecording() {return QVariant::fromValue(this->m_videoRecording);}

    void process();
    double m_coeff;
    QString m_path;
    QStringList file_list;
    StitchThread thread;
    bool m_videoRecording = false;
    QVariant m_stitchProgress = 0;
    QString m_log = "";
    int video_number = 0;

signals:
    void coeffChanged();
    void progressChanged();
    void logChanged();
    void recordingChanged();


public slots:
    void setProgress(QVariant value);
    void addLog(QVariant str);
    Q_INVOKABLE void writeVideo();
};

#endif // INPUTDATA_H
