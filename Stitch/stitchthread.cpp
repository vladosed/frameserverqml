#include "stitchthread.h"

void StitchThread::setData(QStringList list, QString path, double coeff)
{
    this->file_list = list;
    this->m_path = path;
    this->m_coeff = coeff;
}

StitchThread::StitchThread(QObject *parent) : QObject(parent)
{

}

void StitchThread::calculate()
{

    startLog("Процесс сшивки запущен:\n\n");

    for (int i = 1; i < this->file_list.size(); i ++){

        QString first_image = this->m_path + "/" + this->file_list.at(i-1);
        QString second_image = this->m_path + "/" + this->file_list.at(i);

        QTime t;
        t.start();

        Stitch stitch(first_image.toStdString(), second_image.toStdString(), this->m_coeff);
        stitch.calculate();

        QString s_log("");
        s_log.append(first_image + "\n" + second_image + "\n");
        double elapsed = t.elapsed() / 1000.0;
        s_log.append("Сшито за " + QString::number(elapsed) + " сек \n\n\n");

        log(s_log);

        double progress = i / (this->file_list.size() - 1.0);
        emit progressChanged(QVariant::fromValue(progress));

    }
    endLog("Завершено\n\n");



}

void StitchThread::startLog(QString str)
{
    this->inner_log.clear();
    this->inner_log.append(str);
    emit logChanged(QVariant(str));
}

void StitchThread::log(QString str)
{
    this->inner_log.append(str);
    emit logChanged(QVariant(str));
}

void StitchThread::endLog(QString str)
{
    this->inner_log.append(str);



    QString path= QCoreApplication::applicationDirPath() + QString("/last_log.txt");
    QFile file(path);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        file.close();
        emit logChanged(QVariant("log файл. ОШИБКА записи\n\n"));
    } else {
        QTextStream out(&file); out << this->inner_log;
        file.close();
        emit logChanged(QVariant("log файл обновлен\n\n"));
    }
    emit logChanged(QVariant(str));

}

