﻿#include "Stitch.h"

Stitch::Stitch(const string & first_image, const string & second_image, double coef)
{
	this->m_first_image = first_image;
	this->m_second_image = second_image;

	auto lastindex = first_image.find_last_of(".");
    this->m_input_georeference = first_image.substr(0, lastindex) + ".@@@";

	lastindex = second_image.find_last_of(".");
	this->m_output_georeference = second_image.substr(0, lastindex) + ".@@@";

	this->coef = coef;

    cout << first_image << " " << second_image << " " <<  this->m_input_georeference << " " << this->m_output_georeference << endl;

}

Stitch::~Stitch()
{
}

Mat translateImg(Mat &img, int offsetx, int offsety)
{
	Mat trans_mat = (Mat_<double>(2, 3) << 1, 0, offsetx, 0, 1, offsety);

	warpAffine(img, img, trans_mat, Size(3 * img.cols, 3 * img.rows)); // 3,4 is usual
	return trans_mat;
}

void Stitch::warp_crops(Mat& im_1, const Mat& im_2)
{
	int minHessian = 400;
	Ptr<SURF> f2d = SURF::create(minHessian);


	// Step 1: Detect the keypoints:
	std::vector<KeyPoint> keypoints_1, keypoints_2;
	f2d->detect(im_1, keypoints_1);
	f2d->detect(im_2, keypoints_2);

	// Step 2: Calculate descriptors (feature vectors)
	Mat descriptors_1, descriptors_2;
	f2d->compute(im_1, keypoints_1, descriptors_1);
	f2d->compute(im_2, keypoints_2, descriptors_2);

	// Step 3: Matching descriptor vectors using BFMatcher :
	BFMatcher matcher;
	std::vector< DMatch > matches;
	matcher.match(descriptors_1, descriptors_2, matches);

	// Keep best matches only to have a nice drawing.
	// We sort distance between descriptor matches
	Mat index;
	int nbMatch = int(matches.size());
	Mat tab(nbMatch, 1, CV_32F);
	for (int i = 0; i < nbMatch; i++)
		tab.at<float>(i, 0) = matches[i].distance;
	sortIdx(tab, index, SORT_EVERY_COLUMN + SORT_ASCENDING);
	vector<DMatch> bestMatches;

	for (int i = 0; i < 200; i++)
        bestMatches.push_back( matches[index.at < int >(i, 0)] );


	// 1st image is the destination image and the 2nd image is the src image


	for (vector<DMatch>::iterator it = bestMatches.begin(); it != bestMatches.end(); ++it) {
		//cout << it->queryIdx << "\t" << it->trainIdx << "\t" << it->distance << "\n";
		//-- Get the keypoints from the good matches
		dst_pts.push_back(keypoints_1[it->queryIdx].pt);
		source_pts.push_back(keypoints_2[it->trainIdx].pt);
	}

}

void Stitch::getCoordsFromFile(vector<Point2f>& points1, vector<Point2f>& points2)
{
	ifstream geoFile;
    geoFile.open(this->m_input_georeference);
	if (!geoFile) {
		cerr << "Unable to open file " << this->m_input_georeference;
		exit(1);  
	}

	int i = 1;
	for (std::string line; std::getline(geoFile, line); i++)   //read stream line by line
	{
		if (i <= 2) //skip 2 lines
			continue;

		std::istringstream in(line);

		float x, y, lat, lon;
		in >> x >> y >> lat >> lon;

		points1.push_back(Point2f(x, y));
		points2.push_back(Point2f(lat, lon));
	}
}

void Stitch::writeCoordsToFile(vector<Point2f>& points1, vector<Point2f>& points2)
{
	ofstream outputFile;
    outputFile.open(this->m_output_georeference);
	outputFile << "0 0 0 # 0 0 0 \n";
	outputFile << "1 9 29 # 0 0 0 \n";

	if (points1.size() != points2.size())
		return;

	for (int i = 0; i < points1.size(); i++) {
		outputFile << points1.at(i).x << " \t" << points1.at(i).y << "\t" << points2.at(i).x << "\t" << points2.at(i).y << endl;
	}

	outputFile.close();
}

void Stitch::resizeCoordinats(vector<Point2f>& points, double coeff)
{
	for (auto &point : points) {
		point = point * coeff;
	}

}

void Stitch::displayJSON(vector<Point2f>& points)
{
	for (auto item : points) {
		cout << "{ lat: " << item.x << ", lng: " << item.y << "}," << endl;
	}
}


void Stitch::calculate()
{
	clock_t begin = clock();

    vector<Point2f> base_points_for_picture, current_points_for_picture, base_resized_for_picture, final_pts;

	Mat picture_1, picture_2, current_image_1, current_image_2, picture_3;

	getCoordsFromFile(base_points_for_picture, current_points_for_picture);
	base_resized_for_picture = base_points_for_picture;


	Mat wim_2;
    picture_1 = imread(this->m_first_image);
	cv::resize(picture_1, picture_1, cv::Size(), this->coef, this->coef);
	this->resizeCoordinats(base_resized_for_picture, coef);



    cout << base_resized_for_picture;



    picture_2 = imread(this->m_second_image);
	cv::resize(picture_2, picture_2, cv::Size(), this->coef, this->coef);

	vector<Point2f> scene_corners_vector, test_vector, dst_pts_corners_vector, source_pts_corners_vector;



	Mat H = findHomography(base_resized_for_picture, current_points_for_picture, RANSAC);
	warp_crops(picture_1, picture_2);

	Mat H1_2 = findHomography(source_pts, dst_pts, RANSAC);
	perspectiveTransform(base_resized_for_picture, source_pts_corners_vector, H1_2);


    cout << H << endl;

    cout << H1_2 << endl;


    perspectiveTransform(source_pts_corners_vector, final_pts, H);

    qDebug() <<"hello";

	displayJSON(source_pts_corners_vector);

    writeCoordsToFile(base_points_for_picture, final_pts);



	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

	cout << "Success: " << elapsed_secs << endl;

	waitKey(0);

}
