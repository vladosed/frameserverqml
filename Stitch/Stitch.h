#pragma once

#include <QObject>
#include <QDebug>

#include <iostream>
#include "opencv2/core.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include <opencv2/opencv.hpp>
#include <opencv2/stitching.hpp>


using namespace cv;
using namespace cv::xfeatures2d;
using namespace std;

class Stitch : public QObject
{
    Q_OBJECT
public:
    explicit Stitch(const string &first_image, const string &second_image, double coef);
	~Stitch();
	void calculate();
	void warp_crops(Mat& im_1, const Mat& im_2);

	void getCoordsFromFile(vector<Point2f> &points1, vector<Point2f> & points2);
	void writeCoordsToFile(vector<Point2f> &points1, vector<Point2f> & points2);

	void resizeCoordinats(vector<Point2f> &points1, double coeff);
	void displayJSON(vector<Point2f> &points1);
private:
	vector<Point2f> dst_pts;                   
	vector<Point2f> source_pts;                

	string m_first_image, m_second_image, m_input_georeference, m_output_georeference;
	double coef = 1;
};

