import QtQuick 2.12
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.12

Item {
    ColumnLayout{
        spacing: 2
        anchors.fill: parent

        Rectangle {
            id: startVideo

            color: inputData.v_recording ? "green" : "transparent"
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: parent.height / 4
            Text{
                text: "Запись видео"
                font.pointSize: 18
                anchors.top: parent.top
                anchors.topMargin: 20
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Button{
                text: inputData.v_recording ? "Завершить" : "Начать"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                onClicked: {
                    inputData.writeVideo();
                }
            }
        }

        Rectangle {
            id: setDirectory
            Layout.preferredWidth: parent.width
            //Layout.preferredHeight:
            Layout.preferredHeight: parent.height * 0.3
            color: "transparent"
            border.width: 2
            border.color: "black"
            Text{
                text: "Surf"
                font.pointSize: 24
                anchors.top: parent.top
                anchors.topMargin: 20
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Button{
                text: "Укажите файл"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                onClicked: {
                    fileDialog.open();
                }
            }
            ProgressBar {
                width:parent.width - 10
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                value: inputData.stitchProgress
            }
        }
    }

}
