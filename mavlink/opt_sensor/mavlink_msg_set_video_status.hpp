// MESSAGE SET_VIDEO_STATUS support class

#pragma once

namespace mavlink {
namespace opt_sensor {
namespace msg {

/**
 * @brief SET_VIDEO_STATUS message
 *
 * Start video recording
 */
struct SET_VIDEO_STATUS : mavlink::Message {
    static constexpr msgid_t MSG_ID = 1;
    static constexpr size_t LENGTH = 2;
    static constexpr size_t MIN_LENGTH = 2;
    static constexpr uint8_t CRC_EXTRA = 13;
    static constexpr auto NAME = "SET_VIDEO_STATUS";


    char status; /*<  status */
    char camera; /*<  camera */


    inline std::string get_name(void) const override
    {
            return NAME;
    }

    inline Info get_message_info(void) const override
    {
            return { MSG_ID, LENGTH, MIN_LENGTH, CRC_EXTRA };
    }

    inline std::string to_yaml(void) const override
    {
        std::stringstream ss;

        ss << NAME << ":" << std::endl;
        ss << "  status: " << +status << std::endl;
        ss << "  camera: " << +camera << std::endl;

        return ss.str();
    }

    inline void serialize(mavlink::MsgMap &map) const override
    {
        map.reset(MSG_ID, LENGTH);

        map << status;                        // offset: 0
        map << camera;                        // offset: 1
    }

    inline void deserialize(mavlink::MsgMap &map) override
    {
        map >> status;                        // offset: 0
        map >> camera;                        // offset: 1
    }
};

} // namespace msg
} // namespace opt_sensor
} // namespace mavlink
