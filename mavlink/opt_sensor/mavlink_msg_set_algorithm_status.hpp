// MESSAGE SET_ALGORITHM_STATUS support class

#pragma once

namespace mavlink {
namespace opt_sensor {
namespace msg {

/**
 * @brief SET_ALGORITHM_STATUS message
 *
 * Run specific algorithm
 */
struct SET_ALGORITHM_STATUS : mavlink::Message {
    static constexpr msgid_t MSG_ID = 2;
    static constexpr size_t LENGTH = 3;
    static constexpr size_t MIN_LENGTH = 3;
    static constexpr uint8_t CRC_EXTRA = 140;
    static constexpr auto NAME = "SET_ALGORITHM_STATUS";


    char algorithm; /*<  algorithm */
    char status; /*<  status */
    char camera; /*<  camera */


    inline std::string get_name(void) const override
    {
            return NAME;
    }

    inline Info get_message_info(void) const override
    {
            return { MSG_ID, LENGTH, MIN_LENGTH, CRC_EXTRA };
    }

    inline std::string to_yaml(void) const override
    {
        std::stringstream ss;

        ss << NAME << ":" << std::endl;
        ss << "  algorithm: " << +algorithm << std::endl;
        ss << "  status: " << +status << std::endl;
        ss << "  camera: " << +camera << std::endl;

        return ss.str();
    }

    inline void serialize(mavlink::MsgMap &map) const override
    {
        map.reset(MSG_ID, LENGTH);

        map << algorithm;                     // offset: 0
        map << status;                        // offset: 1
        map << camera;                        // offset: 2
    }

    inline void deserialize(mavlink::MsgMap &map) override
    {
        map >> algorithm;                     // offset: 0
        map >> status;                        // offset: 1
        map >> camera;                        // offset: 2
    }
};

} // namespace msg
} // namespace opt_sensor
} // namespace mavlink
