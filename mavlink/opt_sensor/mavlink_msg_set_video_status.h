#pragma once
// MESSAGE SET_VIDEO_STATUS PACKING

#define MAVLINK_MSG_ID_SET_VIDEO_STATUS 1

MAVPACKED(
typedef struct __mavlink_set_video_status_t {
 char status; /*<  status*/
 char camera; /*<  camera*/
}) mavlink_set_video_status_t;

#define MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN 2
#define MAVLINK_MSG_ID_SET_VIDEO_STATUS_MIN_LEN 2
#define MAVLINK_MSG_ID_1_LEN 2
#define MAVLINK_MSG_ID_1_MIN_LEN 2

#define MAVLINK_MSG_ID_SET_VIDEO_STATUS_CRC 13
#define MAVLINK_MSG_ID_1_CRC 13



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_SET_VIDEO_STATUS { \
    1, \
    "SET_VIDEO_STATUS", \
    2, \
    {  { "status", NULL, MAVLINK_TYPE_CHAR, 0, 0, offsetof(mavlink_set_video_status_t, status) }, \
         { "camera", NULL, MAVLINK_TYPE_CHAR, 0, 1, offsetof(mavlink_set_video_status_t, camera) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_SET_VIDEO_STATUS { \
    "SET_VIDEO_STATUS", \
    2, \
    {  { "status", NULL, MAVLINK_TYPE_CHAR, 0, 0, offsetof(mavlink_set_video_status_t, status) }, \
         { "camera", NULL, MAVLINK_TYPE_CHAR, 0, 1, offsetof(mavlink_set_video_status_t, camera) }, \
         } \
}
#endif

/**
 * @brief Pack a set_video_status message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param status  status
 * @param camera  camera
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_set_video_status_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               char status, char camera)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN];
    _mav_put_char(buf, 0, status);
    _mav_put_char(buf, 1, camera);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN);
#else
    mavlink_set_video_status_t packet;
    packet.status = status;
    packet.camera = camera;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_SET_VIDEO_STATUS;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_SET_VIDEO_STATUS_MIN_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_CRC);
}

/**
 * @brief Pack a set_video_status message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param status  status
 * @param camera  camera
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_set_video_status_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   char status,char camera)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN];
    _mav_put_char(buf, 0, status);
    _mav_put_char(buf, 1, camera);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN);
#else
    mavlink_set_video_status_t packet;
    packet.status = status;
    packet.camera = camera;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_SET_VIDEO_STATUS;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_SET_VIDEO_STATUS_MIN_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_CRC);
}

/**
 * @brief Encode a set_video_status struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param set_video_status C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_set_video_status_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_set_video_status_t* set_video_status)
{
    return mavlink_msg_set_video_status_pack(system_id, component_id, msg, set_video_status->status, set_video_status->camera);
}

/**
 * @brief Encode a set_video_status struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param set_video_status C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_set_video_status_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_set_video_status_t* set_video_status)
{
    return mavlink_msg_set_video_status_pack_chan(system_id, component_id, chan, msg, set_video_status->status, set_video_status->camera);
}

/**
 * @brief Send a set_video_status message
 * @param chan MAVLink channel to send the message
 *
 * @param status  status
 * @param camera  camera
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_set_video_status_send(mavlink_channel_t chan, char status, char camera)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN];
    _mav_put_char(buf, 0, status);
    _mav_put_char(buf, 1, camera);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SET_VIDEO_STATUS, buf, MAVLINK_MSG_ID_SET_VIDEO_STATUS_MIN_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_CRC);
#else
    mavlink_set_video_status_t packet;
    packet.status = status;
    packet.camera = camera;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SET_VIDEO_STATUS, (const char *)&packet, MAVLINK_MSG_ID_SET_VIDEO_STATUS_MIN_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_CRC);
#endif
}

/**
 * @brief Send a set_video_status message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_set_video_status_send_struct(mavlink_channel_t chan, const mavlink_set_video_status_t* set_video_status)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_set_video_status_send(chan, set_video_status->status, set_video_status->camera);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SET_VIDEO_STATUS, (const char *)set_video_status, MAVLINK_MSG_ID_SET_VIDEO_STATUS_MIN_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_CRC);
#endif
}

#if MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_set_video_status_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  char status, char camera)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_char(buf, 0, status);
    _mav_put_char(buf, 1, camera);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SET_VIDEO_STATUS, buf, MAVLINK_MSG_ID_SET_VIDEO_STATUS_MIN_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_CRC);
#else
    mavlink_set_video_status_t *packet = (mavlink_set_video_status_t *)msgbuf;
    packet->status = status;
    packet->camera = camera;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SET_VIDEO_STATUS, (const char *)packet, MAVLINK_MSG_ID_SET_VIDEO_STATUS_MIN_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN, MAVLINK_MSG_ID_SET_VIDEO_STATUS_CRC);
#endif
}
#endif

#endif

// MESSAGE SET_VIDEO_STATUS UNPACKING


/**
 * @brief Get field status from set_video_status message
 *
 * @return  status
 */
static inline char mavlink_msg_set_video_status_get_status(const mavlink_message_t* msg)
{
    return _MAV_RETURN_char(msg,  0);
}

/**
 * @brief Get field camera from set_video_status message
 *
 * @return  camera
 */
static inline char mavlink_msg_set_video_status_get_camera(const mavlink_message_t* msg)
{
    return _MAV_RETURN_char(msg,  1);
}

/**
 * @brief Decode a set_video_status message into a struct
 *
 * @param msg The message to decode
 * @param set_video_status C-struct to decode the message contents into
 */
static inline void mavlink_msg_set_video_status_decode(const mavlink_message_t* msg, mavlink_set_video_status_t* set_video_status)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    set_video_status->status = mavlink_msg_set_video_status_get_status(msg);
    set_video_status->camera = mavlink_msg_set_video_status_get_camera(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN? msg->len : MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN;
        memset(set_video_status, 0, MAVLINK_MSG_ID_SET_VIDEO_STATUS_LEN);
    memcpy(set_video_status, _MAV_PAYLOAD(msg), len);
#endif
}
