/** @file
 *	@brief MAVLink comm protocol generated from opt_sensor.xml
 *	@see http://mavlink.org
 */

#pragma once

#include <array>
#include <cstdint>
#include <sstream>

#ifndef MAVLINK_STX
#define MAVLINK_STX 253
#endif

#include "../message.hpp"

namespace mavlink {
namespace opt_sensor {

/**
 * Array of msg_entry needed for @p mavlink_parse_char() (trought @p mavlink_get_msg_entry())
 */
constexpr std::array<mavlink_msg_entry_t, 3> MESSAGE_ENTRIES {{ {0, 103, 179, 0, 0, 0}, {1, 13, 2, 0, 0, 0}, {2, 140, 3, 0, 0, 0} }};

//! MAVLINK VERSION
constexpr auto MAVLINK_VERSION = 3;


// ENUM DEFINITIONS




} // namespace opt_sensor
} // namespace mavlink

// MESSAGE DEFINITIONS
#include "./mavlink_msg_test_types.hpp"
#include "./mavlink_msg_set_video_status.hpp"
#include "./mavlink_msg_set_algorithm_status.hpp"

// base include

