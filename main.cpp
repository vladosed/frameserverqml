#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <Link/communicator.h>
#include "ImageServer/Clients/convolutionmodule.h"
#include "ImageServer/server.h"

#include<Stitch/inputdata.h>

int main(int argc, char *argv[])
{

    Server imageServer;

    InputData inputData;

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    Communicator comm;

    //inputData.writeVideo();

    QObject::connect(&comm, &Communicator::startVideo, &inputData,
            &InputData::writeVideo);
    QObject::connect(&comm, &Communicator::stopVideo, &inputData,
            &InputData::writeVideo);

    QQmlApplicationEngine engine;

    QQmlContext * ctx = engine.rootContext();
    ctx->setContextProperty("inputData", &inputData);
    ctx->setContextProperty("communicator", &comm);
    ctx->setContextProperty("imageServer", &imageServer);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
