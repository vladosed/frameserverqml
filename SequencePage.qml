import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Item {
    ColumnLayout{
        anchors.fill: parent
        Text{
            Layout.alignment: Qt.AlignHCenter
            text: "Межкадровая"
            font.pointSize: 14
        }

        TabBar {
            id: bar
            Layout.preferredWidth: parent.width
            TabButton {
                text: qsTr("Подловченко")
            }
            TabButton {
                text: qsTr("Буздин")
            }
        }

        StackLayout {
            Layout.preferredWidth: parent.width
            currentIndex: bar.currentIndex
            Item {
                id: ltmTab
                Button{
                    anchors.centerIn: parent
                    text:"Открыть окно"
                    onClicked: {
                        imageServer.startVideoStreaming();
                    }
                }
            }
            Item {
                id: buzdinTab
                Column{
                    anchors.margins: 10
                    anchors.fill: parent
                    Text{
                        text: "Алгоритм"
                        font.pointSize: 10
                    }

                    ComboBox{
                        width: parent.width
                        model: ['SURF', 'ORB', 'KLT']
                    }
                }
            }
        }
    }


}
