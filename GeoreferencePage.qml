import QtQuick 2.0

import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

Item {
    ColumnLayout{
        anchors.fill: parent
        Text{
            Layout.alignment: Qt.AlignHCenter
            text: "К подложке"
            font.pointSize: 14
        }

        TabBar {
            id: refBar
            Layout.preferredWidth: parent.width
            TabButton {
                text: qsTr("Подловченко")
            }
            TabButton {
                text: qsTr("Беклемишев")
            }
            TabButton {
                text: qsTr("Буздин")
            }
        }

        StackLayout {
            Layout.preferredWidth: parent.width
            currentIndex: refBar.currentIndex
            Item {
                Button{
                    anchors.centerIn: parent
                    text:"Открыть окно"
                }
            }
            Item {
                Row{
                    spacing: 10
                    anchors.centerIn: parent
                    Button{
                        text:"Подложка"
                        onClicked: {
                            fileMap.open()
                        }
                    }
                    Button{
                        id:selectImages
                        text:"Изображения"
                        onClicked: {
                            filePhotos.open()
                        }
                        enabled: false
                    }
                    Button{
                        id: calculateBek
                        text:"Рассчитать"
                        onClicked: {
                            imageServer.startBekPostProc(fileMap.fileUrl, filePhotos.fileUrls)
                        }
                        enabled: false
                    }
                    Text {
                        text: imageServer.bekProgressStatus;
                    }
                }
            }
            Item {
                Column{
                    anchors.margins: 10
                    anchors.fill: parent
                    Text{
                        text: "Алгоритм"
                        font.pointSize: 10
                    }

                    ComboBox{
                        width: parent.width
                        model: ['SURF', 'SURF + Neural Network', 'SURF + Canny']
                    }
                }
            }
        }
    }

    FileDialog {
        id: fileMap
        title: "Выберите подложку"
        folder: shortcuts.home
        nameFilters: [ "Image files (*.jpg *.png *.tiff)", "All files (*)" ]
        onAccepted: {
            selectImages.enabled = true
            console.log("You chose: " + fileMap.fileUrl)
        }
        onRejected: {
        }
        Component.onCompleted: visible = false
    }

    FileDialog {
        id: filePhotos
        title: "Выберите подложку"
        folder: shortcuts.home
        nameFilters: [ "Image files (*.jpg *.png *.tiff)", "All files (*)" ]
        selectMultiple: true
        onAccepted: {
            calculateBek.enabled = true
            console.log("You chose: " + filePhotos.fileUrls)
        }
        onRejected: {
        }
        Component.onCompleted: visible = false
    }
}
