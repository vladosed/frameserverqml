#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H

#include <QObject>
#include <QUdpSocket>
#include <QDebug>
#include "mavlink/opt_sensor/mavlink.h"

class Communicator : public QObject
{
    Q_OBJECT
public:
    explicit Communicator(QObject *parent = nullptr);
    Q_INVOKABLE void helloUDP();

signals:

public slots:
    void readyRead();

private:
    QUdpSocket *socket;

signals:
    void startVideo();
    void stopVideo();

};

#endif // COMMUNICATOR_H
