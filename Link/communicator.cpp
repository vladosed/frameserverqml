#include "communicator.h"

Communicator::Communicator(QObject *parent) : QObject(parent)
{
    socket = new QUdpSocket(this);

    // The most common way to use QUdpSocket class is
    // to bind to an address and port using bind()
    // bool QAbstractSocket::bind(const QHostAddress & address,
    //     quint16 port = 0, BindMode mode = DefaultForPlatform)
    socket->bind(QHostAddress::LocalHost, 7755);

    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));

}

void Communicator::helloUDP()
{
    qDebug() << "hello";
    QByteArray Data;
    Data.append("Hello from UDP");

    socket->writeDatagram(Data, QHostAddress::LocalHost, 7755);
}

void Communicator::readyRead()
{
    QByteArray data;
    data.resize(socket->pendingDatagramSize());

    QHostAddress sender;
    quint16 senderPort;

    socket->readDatagram(data.data(), data.size(),
                         &sender, &senderPort);

    mavlink_message_t message;
    mavlink_status_t status;


    for (int pos = 0; pos < data.length(); ++pos)
    {
        if (mavlink_parse_char(MAVLINK_COMM_0, (uint8_t)data[pos],
                               &message, &status))
        {
            //emit messageReceived(message);
            if (message.msgid == MAVLINK_MSG_ID_SET_VIDEO_STATUS)
            {
                qDebug() << "MAVLINK_MSG_ID_SET_VIDEO_STATUS";

                mavlink_set_video_status_t v_status;
                mavlink_msg_set_video_status_decode(&message, &v_status);
                if(v_status.status == 0){
                    qDebug() << "Остановить запись видео";
                    emit stopVideo();
                }
                if(v_status.status == 1){
                    qDebug() << "Начать запись видео";
                    emit startVideo();
                }
            }
        }
    }


    qDebug() << "Message from: " << sender.toString();
    qDebug() << "Message port: " << senderPort;
    qDebug() << "Message: " << data;
}
