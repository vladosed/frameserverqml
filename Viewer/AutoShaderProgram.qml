import Qt3D.Render 2.0



ShaderProgram {
    vertexShaderCode: loadSource("qrc:/Viewer/shaders/texture.vert")
    fragmentShaderCode: loadSource("qrc:/Viewer/shaders/texture.frag")
}
