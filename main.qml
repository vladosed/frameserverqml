import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.3
import Qt.labs.folderlistmodel 2.2
import Qt.labs.settings 1.0

ApplicationWindow {
    visible: true
    width: 1280
    height: 720

    Timer {
        interval: 60000
        ; running: true; repeat: true
        onTriggered: {
            if(inputData.v_recording){
                inputData.writeVideo(); // stop recording
                inputData.writeVideo(); // start recording
            }
        }
    }

    Rectangle{
        id: menu
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: parent.width * 0.3
        border.width: 2
        border.color: "black"
        color: "white"

        TabBar {
            id: bar
            width: parent.width
            anchors.margins: 2
            TabButton {
                text: qsTr("Пост\nобработка")
            }
            TabButton {
                text: qsTr("Скрипты\n(временно)")
            }
            TabButton {
                text: qsTr("Настройки\nотображения")
                enabled: false
            }
        }

        StackLayout {
            anchors.bottom: parent.bottom
            anchors.top: bar.bottom
            width: parent.width
            currentIndex: bar.currentIndex
            PostProcessPage{
                anchors.fill:parent
            }
            RealProcessPage{
                anchors.fill:parent
            }
            Item {
                id: activityTab
            }
        }
    }
    Viewer{
        anchors.top: parent.top
        anchors.left: menu.right
        anchors.bottom: parent.bottom
        anchors.right: parent.right
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: shortcuts.home
        //selectFolder: true
        onAccepted: {
            console.log("You chose: " + fileDialog.folder)
            inputData.parseFile(fileDialog.fileUrl);
            //folderModel.folder = folder
        }
        onRejected: {

        }
        //Component.onCompleted: visible = true
    }


    Settings {
        property alias folder: fileDialog.folder
    }

}
