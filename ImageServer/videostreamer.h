#ifndef VIDEOSTREAMER_H
#define VIDEOSTREAMER_H

#include <QObject>
#include <QFutureWatcher>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

#include "Clients/convolutionmodule.h"
#include "Clients/ltmmodule.h"



class VideoStreamer : public QObject
{
    Q_OBJECT
public:
    explicit VideoStreamer(QObject *parent = nullptr);
    ~VideoStreamer();
    void startStream();
    void stopStream();
    void capture();
    void broadcast(int msec);
    void toLtmClient();
    void startBek(QString map, QStringList listImages);
    void bekPostProc();

signals:
    void BekProgress(int max, int current);

public slots:

private:
    ConvolutionModule convModule;
    LtmModule ltmModule;
    cv::VideoCapture m_cap;
    cv::Mat frame;
    bool videoCapturing = false;
    tagBITMAP * bitmap;

    QString map_name;
    QStringList photo_list;
};

#endif // VIDEOSTREAMER_H
