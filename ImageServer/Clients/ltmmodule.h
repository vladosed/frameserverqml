#ifndef LTMMODULE_H
#define LTMMODULE_H

#include <QObject>
#include "LtmClient/LtmClient.h"
#include "LtmClient/win_bitmap.h"
#include <QDebug>
#include <QFile>
#include <QtMath>

#include "opencv2/core.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

class TelemPoint{
public:
    TelemPoint(double msec,
               double lat,
               double lon,
               double alt,
               double gam,
               double tet,
               double psi);
    TelemPoint(QStringList list);

    double getMSec() const {return m_msec;}
    double getLat() const {return m_lat;}
    double getLon() const {return m_lon;}
    double getAlt() const {return m_alt;}
    double getGam() const {return m_gam;}
    double getTet() const {return m_tet;}
    double getPsi() const {return m_psi;}

    void debug();

private:
    double m_msec;
    double m_lat;
    double m_lon;
    double m_alt;
    double m_gam;
    double m_tet;
    double m_psi;


};

class LtmModule : public QObject
{
    Q_OBJECT
public:
    explicit LtmModule(QObject *parent = nullptr);
    Q_INVOKABLE void showWindow();
    Q_INVOKABLE void nextStep(int millis, const cv::Mat &frame);
    Q_INVOKABLE void loadTelemetry(QString filename);


signals:

public slots:
private:
    tagBITMAP * m_bitmap;
    QList<TelemPoint> telemPoints;
};

#endif // LTMMODULE_H
