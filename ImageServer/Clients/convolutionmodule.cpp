#include "convolutionmodule.h"

/******************************/
// Отладочная печать
#if 0
void q1_debug(char *s)
{
    static long count = 0;
    FILE *f1;

    if (count == 0)
        remove("ggg");
    count++;

    f1 = fopen("ggg", "a");
    if (f1 == NULL)
        f1 = fopen("ggg", "w");
    if (f1)
    {
        fprintf(f1, "%s\n", s);
        fclose(f1);
    }
}
#endif

#if 1
void q1_debug(char *s)
{
    printf("%s\n", s);
    fflush(stdout);
}
#endif

/**********************************/
//  3 байта на пиксель  <--> 1 байт на пиксель
//   NOTE: в установках транслятора /J т.е char = unsigned char

void tu_buf_3v1(char *mem1, long nx, long sy)
{
    long nn = nx * sy;
    unsigned long ul;
    long i;

    for (i = 0; i < nn; i++)
    {
        ul = (unsigned)(mem1[3 * i + 0]) +
            (unsigned)(mem1[3 * i + 1]) +
            (unsigned)(mem1[3 * i + 2]);
        mem1[i] = (unsigned char)(ul / 3);
    } /* i */
} /* end tu_buf_3v1 */

void tu_buf_1v3(char *mem1, long nx, long sy)
{
    long nn = nx * sy;
    long i;

    for (i = nn - 1; i >= 0; i--)
    {
        mem1[3 * i + 2] = mem1[3 * i + 1] = mem1[3 * i + 0] = mem1[i];
    } /* i */
} /* end tu_buf_1v3 */

// Найти последнюю точку в имени файла (= начало расширения)
long q1_find_last_point(char *msg)
{
    long i = 0;

    while (msg[i])
        i++;
    if (i == 0)
        return (-1);
    while (msg[i] != '.')
    {
        if (msg[i] == 92)
            return (-1);
        if (msg[i] == ':')
            return (-1);
        i--;
        if (i < 0)
            return (i);
    }
    return (i);
} /* end q1_find_last_point */

/**************************/
// Функции загрузки растра из файла в массив в памяти и обратно -
//  оболочки над ф-ями из RASTER.DLL
/**************************/

static
HINSTANCE rs_lib = NULL;

static
long load_rs_lib()
{
    long ret = -1;

    if (rs_lib == NULL)
    {
        rs_lib = LoadLibraryA("raster.dll");
        CHK(rs_lib == NULL);
    } /* endif rs_lib == NULL */

    ret = 0;
end:
    return ret;
} /* end load_rs_lib */

#define FZ1(p, q, r)                                             \
IRasterUtil* __stdcall p q                                       \
  {                                                              \
  IRasterUtil *ret = NULL;                                       \
  typedef IRasterUtil*(__cdecl * t_##p) q; static t_##p f_##p;   \
                                                                 \
  if (f_##p == NULL)                                             \
    {                                                            \
    CHK (load_rs_lib() != 0);                                    \
    CHK ((f_##p = (t_##p)GetProcAddress(rs_lib, #p)) == NULL);   \
    }                                                            \
                                                                 \
  ret = f_##p r;                                                 \
  end:                                                           \
  return ret;                                                    \
  } /* FZ1 */

FZ1(tg_get_raster_util, (), ())

long q1_tst_save_block_as_tiff(char *buf, long nx, long ny, char *name)
{
    long ret = -1;
    IRasterUtil *iu = tg_get_raster_util();
    IRaster *r = NULL;

    CHK(iu == NULL);

    CHK(iu->create_black(name, nx, ny, TIF_FORMAT,
        500.0, 0, 0) == FALSE);
    r = iu->open_write(name);
    CHK(r == NULL);
    CHK(r->write_block(buf, nx, ny, 0, 0) == FALSE);

    ret = 0;
end:
    if (r)
        r->release();
    return ret;
} /* end q1_tst_save_block_as_tiff */


long save_mas(char *mas, IXY *size, char *name, RasterFormat format)
{
    long ret = -1;
    IRasterUtil *iu = tg_get_raster_util();
    IRaster *r = NULL;

    CHK(iu->create_black(name, size->x, size->y, format,
        500.0, 0, 0) == FALSE);

    r = iu->open_write(name);
    CHK(r == NULL);
    CHK(r->write_block(mas, size->x, size->y, 0, 0) == FALSE);

    ret = 0;
end:
    if (r)
        r->release();
    return ret;
}

char *mas_read_rgb(char *name, IXY *size)
{
    char *mas = NULL, *ret = NULL;
    IRasterUtil *iu = tg_get_raster_util();
    IRaster *r = NULL;
    long bytes = 0;

    CHK(iu == NULL);
    r = iu->open_read(name);
    CHK(r == NULL);
    CHK(r->get_size(&(size->x), &(size->y)) == FALSE);
    bytes = r->get_bytes_per_pixel();
    CHK(!(bytes == 1 || bytes == 3));

    INIT_PTR(char, mas, 3 * size->x * size->y);
    CHK(r->read_block_1v1(mas, size->x, size->y, 0, 0) == FALSE);

    if (bytes == 1)
        tu_buf_1v3(mas, size->x, size->y);

    ret = mas;
end:
    if (r)
        r->release();
    if (ret == NULL)
        DELETE(mas);
    return ret;
} /* end *mas_read_rgb */

char *mas_read_bw(char *name, IXY *size)
{
    char *mas = NULL, *ret = NULL;
    IRasterUtil *iu = tg_get_raster_util();
    IRaster *r = NULL;
    long bytes = 0;

    CHK(iu == NULL);
    r = iu->open_read(name);
    CHK(r == NULL);
    CHK(r->get_size(&(size->x), &(size->y)) == FALSE);
    bytes = r->get_bytes_per_pixel();
    CHK(!(bytes == 1 || bytes == 3));

    INIT_PTR(char, mas, 3 * size->x * size->y);
    CHK(r->read_block_1v1(mas, size->x, size->y, 0, 0) == FALSE);

    if (bytes == 3)
        tu_buf_3v1(mas, size->x, size->y);

    ret = mas;
end:
    if (r)
        r->release();
    if (ret == NULL)
        DELETE(mas);
    return ret;
} /* end *mas_read_bw */


/****************************/
// Работа с файлами *.@@@
/****************************/
long tri_sobaki_load(char *name, char *str_1, char *str_2, DXY *ugl_pix, DXY *ugl_geo)
{
    long ret = -1;
    FILE *f1 = NULL;
    char s[256];

    f1 = fopen(name, "r");
    CHK(f1 == NULL);
    CHK(fgets(str_1, 256, f1) == NULL);
    CHK(fgets(str_2, 256, f1) == NULL);

    for (long i = 0; i < 4; i++)
    {
        CHK(fgets(s, 256, f1) == NULL);
        CHK(sscanf(s, "%le%le%le%le", &(ugl_pix[i].x), &(ugl_pix[i].y), &(ugl_geo[i].x), &(ugl_geo[i].y)) != 4);
    }


    ret = 0;
end:
    if (f1)
        fclose(f1);
    return ret;
} /* tri_sobaki_load */

long tri_sobaki_save(char *name, char *str_1, char *str_2, DXY *ugl_pix, DXY *ugl_geo)
{
    long ret = -1;
    FILE *f1 = NULL;

    f1 = fopen(name, "w");
    CHK(f1 == NULL);
    fputs(str_1, f1);
    fputs(str_2, f1);

    for (long i = 0; i < 4; i++)
    {
        fprintf(f1, "%lf %lf %lf %lf\n", ugl_pix[i].x, ugl_pix[i].y, ugl_geo[i].x, ugl_geo[i].y);
    }


    ret = 0;
end:
    if (f1)
        fclose(f1);
    return ret;
} /* tri_sobaki_save */

void tri_sobaki_name_by_file_name(char *name_file, char *name_sbk)
{
    long j = q1_find_last_point(name_file);
    sprintf(name_sbk, name_file);
    sprintf(name_sbk + (j + 1), "@@@");
}


long tri_sobaki_load_by_raster_name(char *name, DXY *ugl_pix, DXY *ugl_geo)
{
    char s1[256], s2[256];
    char name_sbk[256];

    tri_sobaki_name_by_file_name(name, name_sbk);
    return tri_sobaki_load(name_sbk, s1, s2, ugl_pix, ugl_geo);
}

long tri_sobaki_create(char *name_ph, IXY *size_ph, char *name_map, DXY *ugl_ph_geo)
{
    long ret = -1;
    char s1[256], s2[256];
    char name_sbk_map[256], name_sbk_ph[256];
    DXY ugl_geo_map[4], ugl_pix_map[4];
    DXY ugl_pix_ph[4];

    tri_sobaki_name_by_file_name(name_map, name_sbk_map);
    tri_sobaki_name_by_file_name(name_ph, name_sbk_ph);

    CHK(tri_sobaki_load(name_sbk_map, s1, s2, ugl_pix_map, ugl_geo_map) != 0);

    SIZE_TO_UGL(size_ph, ugl_pix_ph);
    CHK(tri_sobaki_save(name_sbk_ph, s1, s2, ugl_pix_ph, ugl_ph_geo) != 0);


    ret = 0;
end:
    return ret;
} /* tri_sobaki_create */


/****************************/
void test_load_param()
{
    FindPosIntfs *fpi = NULL;
    char name[100] = "c:\\pnb\\vorobjev\\2019\\fly_pnb.dat";

    DEBUG0("===== test_load_param() =======");

    // Создали объект, вызвав ф-ю из ДЛЛ
    fpi = pnb_find_position_intfs();
    CHK(fpi == NULL);

    CHK(fpi->param_load(name) != 0);

end:
    // Освободили объект
    if (fpi)
        fpi->release();

    DEBUG0("===== END test_load_param() =======");
} /* test_load_param */


ConvolutionModule::ConvolutionModule(QObject *parent) : QObject(parent)
{
    this->testParam();
    //this->compareTwoImages(QString(" "), QString(" "));
}

void ConvolutionModule::testParam()
{
    FindPosIntfs *fpi = NULL;

    qDebug() << "===== test_param() =======";

    // Создали объект, вызвав ф-ю из ДЛЛ
    fpi = pnb_find_position_intfs();
    Q_ASSERT ((fpi == NULL));

    qDebug() << "fpi=" << reinterpret_cast <unsigned>(fpi);

    // Получили номер версии и распечатали
    long iv = fpi->version();
    qDebug() << " version=" << iv;

    // Получили число параметров
    long n = fpi->get_param_count();
    qDebug() << "get_param_count=" << n;

    Q_ASSERT(n < 2);
    // Например возьмем порядковый номер параметра = 1
    long i_param = 1, param_id = 0;
    double dval = 0;
    long ival = 0, type = 0;

    // Получили идентификатор этого параметра
    param_id = fpi->get_param_id(i_param);
    Q_ASSERT ((param_id < 0));
    qDebug() << "param_id=" << param_id;

    // Получили тип параметра
    fpi->get_param_type(param_id, &type);

    // Если DOUBLE, печатаем DOUBLE, если LONG, печатаем LONG, если ни то ни другое, печатаем плохой тип
    if (type == PARAM_TYPE_DOUBLE)
        qDebug() <<" type=PARAM_TYPE_DOUBLE";
    if (type == PARAM_TYPE_LONG)
        qDebug() <<"type=PARAM_TYPE_LONG";
    if (!(type == PARAM_TYPE_LONG || type == PARAM_TYPE_DOUBLE))
         qDebug() << "Bad param type=" << type;
    Q_ASSERT (!(type == PARAM_TYPE_LONG || type == PARAM_TYPE_DOUBLE));

    // Получили имя (комментарий) к параметру и распечатали
    char name[256];
    fpi->get_param_name(param_id, name);
    qDebug() << "name=" << name;


    // Получили значение параметра в переменной dval или ival в зависимости от типа
    //  Фактически он типа DOUBLE
    if (type == PARAM_TYPE_DOUBLE)
    {
        fpi->get_param_val(param_id, &dval);
    }
    if (type == PARAM_TYPE_LONG)
    {
        fpi->get_param_val(param_id, &ival);
    }

    // Распечатали полученное значение
     qDebug() << "  GET: dval=" <<  dval << " ival="<< ival;


    // Изменили полученное значение параметра и установили новое значение
    // Потом обратно получили и распечатали полученное
    if (type == PARAM_TYPE_DOUBLE)
    {
        double dval_1 = dval + 0.1;
        fpi->set_param_val(param_id, &dval_1);
        fpi->get_param_val(param_id, &dval);
    }
    if (type == PARAM_TYPE_LONG)
    {
        long ival_1 = ival + 1;
        fpi->set_param_val(param_id, &ival_1);
        fpi->get_param_val(param_id, &ival);
    }

    qDebug() << " SET: dval=" << dval <<  "  ival=%ld" <<  ival;

    // Освободили объект
    if (fpi)
        fpi->release();

    qDebug() << "END (test_param())";
}

void ConvolutionModule::compareTwoImages(const QString & map , const QString & photo)
{
    FindPosIntfs *fpi = NULL;
    // char * name_map = map.toLatin1().data();
    // char * name_ph = photo.toLatin1().data(); //"D:\\Qt\\workspace\\find_pos2\\fp_test\\Debug\\063.JPG";
    char *mas_ph = NULL;
    IXY size_ph[1];
    DXY ugl_ph_geo[4];

    DEBUG0("===== test_3() =======");

    // Создали объект, вызвав ф-ю из ДЛЛ
    fpi = pnb_find_position_intfs();
    CHK(fpi == NULL);

    qDebug() <<"load_map_by_name...";


    // Загрузили подложку из файла
    CHK(fpi->load_map_by_name(map.toLatin1().data()) != 0);

    qDebug() << "load_map_by_name!";

    // Загрузили снимок в массив в памяти
    mas_ph = mas_read_bw( photo.toLatin1().data(), size_ph);
    if (mas_ph == NULL){
        qDebug() << "mas_ph == NULL";
    }

    DEBUG2("size_ph=%ld %ld", size_ph->x, size_ph->y);

    //  параметры расчета:
    double d_side = 2200, angle = 90.0, c = 0.4, kurs = 0;

    //  - размер кадра в метрах
    CHK(fpi->get_param_val(FP_SIZE_M, &d_side) != 0);
    //  - диапазон поиска по углу
    CHK(fpi->get_param_val(FP_ANGLE, &angle) != 0);
    //  - порог по корреляции
    CHK(fpi->get_param_val(FP_CORREL, &c) != 0);


    CHK(fpi->get_param_val(FP_KURS_GRADUS, &kurs) != 0);

    DEBUG4("d_side=%lf angle=%lf correl=%lf kurs=%lf", d_side, angle, c, kurs);



    // Получили привязку снимка к гео-координатам

    qDebug() << size_ph->x << size_ph->y;

    if (fpi->find_kadr(mas_ph, size_ph->x, size_ph->y, (double *)ugl_ph_geo) != 0)
    {
        qDebug() << "find_kadr error";
    }

    // Распечатали
    DEBUG8("ugl_ph_geo=(%lf %lf) (%lf %lf) (%lf %lf) (%lf %lf)",
            ugl_ph_geo[0].x, ugl_ph_geo[0].y,
            ugl_ph_geo[1].x, ugl_ph_geo[1].y,
            ugl_ph_geo[2].x, ugl_ph_geo[2].y,
            ugl_ph_geo[3].x, ugl_ph_geo[3].y);

    // Записали точки привязки в файл
    CHK(tri_sobaki_create( photo.toLatin1().data(), size_ph, map.toLatin1().data(), ugl_ph_geo) != 0);

end:
    // Освободили объект
    if (fpi)
        fpi->release();
    DELETE(mas_ph);

    DEBUG0("===== END test_3() =======");
}
