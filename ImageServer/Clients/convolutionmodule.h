#ifndef CONVOLUTIONCLIENT_H
#define CONVOLUTIONCLIENT_H

#include <QObject>
#include <QDebug>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/calib3d.hpp"
#include "opencv2/tracking.hpp"
#include <opencv2/videoio.hpp>
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include <opencv2/opencv.hpp>
#include <opencv2/stitching.hpp>

#include "ConvolutionClient/find_pos.h"

#include<stdio.h>
#include<math.h>
#include<malloc.h>
#include<stdlib.h>

#include <Windows.h>

#include "ConvolutionClient/OWN.H"
#include "ConvolutionClient/TG_RASTR.H"
#include "ConvolutionClient/PNB_DBG.H"
#include <QDir>


class ConvolutionModule : public QObject
{
    Q_OBJECT
public:
    explicit ConvolutionModule(QObject *parent = nullptr);
    void testParam();
    void compareTwoImages(const QString &map, const QString &photo);

signals:

public slots:
private:


};

#endif // CONVOLUTIONCLIENT_H
