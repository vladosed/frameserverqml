#ifndef PNB_DBG
#define PNB_DBG

//  �⪫�� �� �� ��� �� �ᥩ ��४�ਨ PNB
#ifndef PNB_NO_DEBUG
//#define PNB_NO_DEBUG
#endif

//extern int TalkaError(char *);
extern void q1_debug(char *);

#ifndef PNB_NO_DEBUG

#define DEBUG0(a) {}
#define DEBUG1(a,b) {char _[1000]; sprintf(_, a,b); q1_debug(_);}
#define DEBUG2(a,b,c) {char _[1000]; sprintf(_, a,b,c); q1_debug(_);}
#define DEBUG3(a,b,c,d) {char _[1000]; sprintf(_, a,b,c,d); q1_debug(_);}
#define DEBUG4(a,b,c,d,e) {char _[1000]; sprintf(_, a,b,c,d,e); q1_debug(_);}
#define DEBUG5(a,b,c,d,e,f) {char _[1000]; sprintf(_, a,b,c,d,e,f); q1_debug(_);}
#define DEBUG6(a,b,c,d,e,f,g) {char _[1000];\
                           sprintf(_, a,b,c,d,e,f,g); q1_debug(_);}
#define DEBUG7(a,b,c,d,e,f,g,h) {char _[1000];\
                           sprintf(_, a,b,c,d,e,f,g,h); q1_debug(_);}
#define DEBUG8(a,b,c,d,e,f,g,h,i) {char _[1000];\
                           sprintf(_, a,b,c,d,e,f,g,h,i); q1_debug(_);}
#define DEBUG9(a,b,c,d,e,f,g,h,i,j) {char _[1000];\
                           sprintf(_, a,b,c,d,e,f,g,h,i,j); q1_debug(_);}
#define DEBUG10(a,b,c,d,e,f,g,h,i,j,k) {char _[1000];\
                           sprintf(_, a,b,c,d,e,f,g,h,i,j,k); q1_debug(_);}

#define DEBUG_N(a, b, c) {char _[15000]; long _1,_2=0;\
for(_1=0;_1<(b);_1++){sprintf(_+_2,a,(c)[_1]);while(_[_2])_2++;} q1_debug(_);}

#else  // ifdef PNB_NO_DEBUG

#define DEBUG0(a)
#define DEBUG1(a,b)
#define DEBUG2(a,b,c)
#define DEBUG3(a,b,c,d)
#define DEBUG4(a,b,c,d,e)
#define DEBUG5(a,b,c,d,e,f)
#define DEBUG6(a,b,c,d,e,f,g)
#define DEBUG7(a,b,c,d,e,f,g,h)
#define DEBUG8(a,b,c,d,e,f,g,h,i)
#define DEBUG9(a,b,c,d,e,f,g,h,i,j)
#define DEBUG10(a,b,c,d,e,f,g,h,i,j,k)

#define DEBUG_N(a, b, c)

#endif


#define DEBUG_RAM(s,r) DEBUG5("%s=%lf %lf %lf %lf",s,(r)[0],(r)[1],(r)[2],(r)[3])
#define DEBUG_RAM_E(s,r) DEBUG5("%s=%le %le %le %le",s,(r)[0],(r)[1],(r)[2],(r)[3])
#define DEBUG_RAM_L(s,r) DEBUG5("%s=%ld %ld %ld %ld",s,(r)[0],(r)[1],(r)[2],(r)[3])

#define CHK(a) if (a) {}
#define CHKS(a) if (a) {}

#define LOCATE_T(type, a, n, mem, memsize) {CHK((memsize) < (long)((n) * sizeof((a)[0])));\
        (a) = (type *)(mem); (mem) += (n) * sizeof((a)[0]);   \
        (memsize) -= (n) * sizeof((a)[0]); }

#define LOCATE(a, n, mem, memsize) {CHK((memsize) < (long)((n) * sizeof((a)[0])));\
        (a) = (void *)(mem); (mem) += (n) * sizeof((a)[0]);   \
        (memsize) -= (n) * sizeof((a)[0]); }


#define DEBUG_AFTR(s, a)                                          \
        DEBUG7("%s=\n         %lf %lf %lf\n         %lf %lf %lf", \
        s,(a)->axx,(a)->axy,(a)->bx,(a)->ayx,(a)->ayy,(a)->by);

#define DEBUG_MATRIX(a, m) DEBUG1("%s", a);             \
DEBUG3("        %lf %lf %lf", m->x->x,m->x->y,m->x->z); \
DEBUG3("        %lf %lf %lf", m->y->x,m->y->y,m->y->z); \
DEBUG3("        %lf %lf %lf", m->z->x,m->z->y,m->z->z);



#ifndef PNB_NO_DEBUG

#define PRINT0(a) TalkaError(a)
#define PRINT1(a,b) {char _[1000]; sprintf(_, a,b); TalkaError(_);}
#define PRINT2(a,b,c) {char _[1000]; sprintf(_, a,b,c); TalkaError(_);}
#define PRINT3(a,b,c,d) {char _[1000]; sprintf(_, a,b,c,d); TalkaError(_);}
#define PRINT4(a,b,c,d,e) {char _[1000]; sprintf(_, a,b,c,d,e); TalkaError(_);}
#define PRINT5(a,b,c,d,e,f) {char _[1000]; sprintf(_, a,b,c,d,e,f); TalkaError(_);}
#define PRINT6(a,b,c,d,e,f,g) {char _[1000];\
                           sprintf(_, a,b,c,d,e,f,g); TalkaError(_);}
#define PRINT7(a,b,c,d,e,f,g,h) {char _[1000];\
                           sprintf(_, a,b,c,d,e,f,g,h); TalkaError(_);}
#define PRINT8(a,b,c,d,e,f,g,h,i) {char _[1000];\
                           sprintf(_, a,b,c,d,e,f,g,h,i); TalkaError(_);}
#define PRINT9(a,b,c,d,e,f,g,h,i,j) {char _[1000];\
                           sprintf(_, a,b,c,d,e,f,g,h,i,j); TalkaError(_);}
#define PRINT10(a,b,c,d,e,f,g,h,i,j,k) {char _[1000];\
                           sprintf(_, a,b,c,d,e,f,g,h,i,j,k); TalkaError(_);}

#else   // ifdef PNB_NO_DEBUG

#define PRINT0(a)
#define PRINT1(a,b)
#define PRINT2(a,b,c)
#define PRINT3(a,b,c,d)
#define PRINT4(a,b,c,d,e)
#define PRINT5(a,b,c,d,e,f)
#define PRINT6(a,b,c,d,e,f,g)
#define PRINT7(a,b,c,d,e,f,g,h)
#define PRINT8(a,b,c,d,e,f,g,h,i)
#define PRINT9(a,b,c,d,e,f,g,h,i,j)
#define PRINT10(a,b,c,d,e,f,g,h,i,j,k)

#endif


#define PRINT_RAM(s,r) PRINT5("%s=%lf %lf %lf %lf",s,(r)[0],(r)[1],(r)[2],(r)[3])


#define DEBUG_REPER(r) {DEBUG3("o=%lf %lf %lf",(r)->o->x,(r)->o->y,(r)->o->z);\
                        DEBUG3("a=%.9lf %.9lf %.9lf",(r)->a->x,(r)->a->y,(r)->a->z);\
                        DEBUG3("u=%.9lf %.9lf %.9lf",(r)->u->x,(r)->u->y,(r)->u->z);\
                        DEBUG3("v=%.9lf %.9lf %.9lf",(r)->v->x,(r)->v->y,(r)->v->z);}


#define T_INIT unsigned t;
#define T_BEGIN {t = q1_get_tick_count();}
#define T_END(name) DEBUG1("   "#name"=%ld", q1_get_tick_count() - t)

#endif

