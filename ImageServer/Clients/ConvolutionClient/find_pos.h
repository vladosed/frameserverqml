#ifndef FIND_POS_H
#define FIND_POS_H

#ifndef FLY_PNB_INFS_EXPORT
#define FLY_PNB_INFS_EXPORT
#endif


// �������������� ���������� �������
enum FindPosParam
  {
  FP_QUALITY = 1,
  FP_CORREL,
  FP_ANGLE,
  FP_SCALE,
  FP_DX,
  FP_DY,
  FP_NX,
  FP_NY,
  FP_KURS_GRADUS,
  FP_SIZE_M,
  FP_REDUCE,

  FP_CANNY_PHOTO,
  FP_G_MAS_PHOTO,
  FP_G_GRAD_PHOTO,
  FP_GMIN_1_PHOTO,
  FP_GMIN_2_PHOTO,

  FP_CANNY_MAP,
  FP_G_MAS_MAP,
  FP_G_GRAD_MAP,
  FP_GMIN_1_MAP,
  FP_GMIN_2_MAP
  }; // FindPosParam


// ���� ����������
#ifndef PARAM_TYPE_LONG
#define PARAM_TYPE_LONG   0x0002
#define PARAM_TYPE_DOUBLE 0x0003
#define PARAM_TYPE_BOOL      0x0201
#endif

#if 0
{FP_QUALITY, PARAM_TYPE_DOUBLE, 0, 2.0, "����� �� ��������, �� 1 �� ������., >2 - ������ ������� ��������"},
{FP_CORREL, PARAM_TYPE_DOUBLE, 0, 0.4, "����� �� ����������, �� 0 �� 1"},
{FP_ANGLE, PARAM_TYPE_DOUBLE, 0, 15.0, "�������� ������ �� ���� (�������) +- �� ��������� ����"},
{FP_SCALE, PARAM_TYPE_DOUBLE, 0, 0.0, "�������� ������ �� ��������. SCALE=0.1 - ������� �������� �� 10% +-"},
{FP_DX, PARAM_TYPE_DOUBLE, 0, 0.5, "��� ������ �� ������ � �������� ������"},
{FP_DY, PARAM_TYPE_DOUBLE, 0, 0.5, "��� ������ �� ������� � �������� ������"},
{FP_NX, PARAM_TYPE_LONG, 0, 0.0, "�������� ������: ����� ����� �� ������ ����� � ������"},
{FP_NY, PARAM_TYPE_LONG, 0, 0.0, "�������� ������: ����� ����� �� ������� ����� � ����"},
{FP_KURS_GRADUS, PARAM_TYPE_DOUBLE, 0, 0.0, "�������� ���� � ��������. ����������� �� ���� ������ �� ����. �� ����� ������ �������"},
{FP_SIZE_M, PARAM_TYPE_DOUBLE, 0, 1200.0, "������ ������ �� ��������� � ������"},
{FP_REDUCE, PARAM_TYPE_LONG, 500, 0.0, "��������� ���������� ������ �� ... �������� �� ������ � �������� ��������������"},
{FP_CANNY_PHOTO, PARAM_TYPE_LONG, 0, 0.0, "������ �� �������������� ������������� ������. 0 - ���, 1 - ��, 2 - �� � ��������� ��������� � �����"},
{FP_G_MAS_PHOTO, PARAM_TYPE_LONG, 0, 5.0, "������������� ������: ����������� ��������� ������ (����� >= 0). 0 - ��� �����������"},
{FP_G_GRAD_PHOTO, PARAM_TYPE_LONG, 0, 5.0, "������������� ������: ����������� �������� � ��������� (����� >= 0). 0 - ��� �����������"},
{FP_GMIN_1_PHOTO, PARAM_TYPE_DOUBLE, 0, 0.05, "������������� ������: ����� 1 ��� ������ �������� (�� 0 �� 1). ��� ������ ����� 1, ��� ������ �������� ��������"},
{FP_GMIN_2_PHOTO, PARAM_TYPE_DOUBLE, 0, 0.1, "������������� ������: ����� 2 ��� ������ �������� (������, ��� ����� 1 � �� 0 �� 1)"},
{FP_CANNY_MAP, PARAM_TYPE_LONG, 0, 0.0, "������ �� �������������� ������������� ��������. 0 - ���, 1 - ��, 2 - �� � ��������� ��������� � �����"},
{FP_G_MAS_MAP, PARAM_TYPE_LONG, 0, 5.0, "������������� ��������: ����������� ��������� ������ (����� >= 0). 0 - ��� �����������"},
{FP_G_GRAD_MAP, PARAM_TYPE_LONG, 0, 5.0, "������������� ��������: ����������� �������� � ��������� (����� >= 0). 0 - ��� �����������"},
{FP_GMIN_1_MAP, PARAM_TYPE_DOUBLE, 0, 0.05, "������������� ��������: ����� 1 ��� ������ �������� (�� 0 �� 1). ��� ������ ����� 1, ��� ������ �������� ��������"},
{FP_GMIN_2_MAP, PARAM_TYPE_DOUBLE, 0, 0.1, "������������� ��������: ����� 2 ��� ������ �������� (������, ��� ����� 1 � �� 0 �� 1)"}
#endif


// ���� �� ��������� ��������, �� ������������ �������� �-��
//  0 - ��� ������,  != 0 - ������
struct FindPosIntfs
  {
  // ���������� ����� ������
  virtual long __stdcall version() = 0;
  // ���������� ������
  virtual void __stdcall release() = 0;

  // ��������� �������� �� ����� ����� � ��������� @@@ ��� TAB
  virtual long __stdcall load_map_by_name(char *name) = 0;

  // ��������� �������� �� ������� 1 ���� �� ������� mas[size_x*size_y]
  //  ugl_geo[8] - �����. ����� ������ �������� �� ��������� (x1, y1) (x2, y2) (x3, y3) (x4, y4)
  //  ������� �� �� �� ��
  virtual long __stdcall load_map_by_mas(char *mas, long size_x, long size_y, double *ugl_geo) = 0;

  // ����� ��������� �����. ����: mas[size_x*size_y] - ����� �����,
  //  �����: ugl_geo_out[8] - ��������� �����. ����� ����� �� ���������
  //  ������� ����� ���������� ���������� �-��
  virtual long __stdcall find_kadr(char *mas, long size_x, long size_y, double *ugl_geo_out) = 0;

  // �������� ��������� �����
  //  ����: mas[size_x*size_y] - ����� �����, ugl_geo_in[8] - ��������� �����. ����� ����� �� ���������
  //  �����: ugl_geo_out[8] - ��������� �����. ����� ����� �� ���������
  //  ������� ����� ���������� ���������� �-��
  virtual long __stdcall update_kadr(char *mas, long size_x, long size_y, double *ugl_geo_in, double *ugl_geo_out) = 0;


  // ������ � ����������
  // �������� ����� ����������
  virtual long __stdcall get_param_count() = 0;
  // �������� ������������� ��������� � ������� i_param (��. FindPosParam)
  virtual long __stdcall get_param_id(long i_param) = 0;
  // �������� ��� ��������� � ��������������� param_id, PARAM_TYPE_LONG, PARAM_TYPE_DOUBLE, ...
  virtual long __stdcall get_param_type(long param_id, long *type) = 0;
  // �������� ��� ��������� � ��������������� param_id, name[256]
  virtual long __stdcall get_param_name(long param_id, char *name) = 0;
  // �������� �������� ��������� � ��������������� param_id
  virtual long __stdcall get_param_val(long param_id, void *val) = 0;
  // ���������� �������� ��������� � ��������������� param_id
  virtual long __stdcall set_param_val(long param_id, void *val) = 0;


  virtual long __stdcall param_load(char *name_file) = 0;
  }; /* FindPosIntfs */

extern "C" FLY_PNB_INFS_EXPORT
FindPosIntfs * __stdcall pnb_find_position_intfs();


#endif
