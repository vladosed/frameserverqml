//---------------------------------------------------------------------------
// 2018-08-07 ����� ��� ������������ ����� (������������)
// �������� ����� � �������� (� ��������� ��������, ���� �������� � �.�.)
//---------------------------------------------------------------------------
#ifndef LtmScannerH
#define LtmScannerH
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
#define LTM_SCAN_STATE_ENABLED 0
#define LTM_SCAN_STATE_FREE 0
#define LTM_SCAN_STATE_BUSY 1
#define LTM_SCAN_STATE_PRE_SCAN 2
#define LTM_SCAN_STATE_RUN_SCAN 3
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
#include "ltm_defs.h"

#include "TimeOut.h"         // TTimeOut
#include "ScanRange.h"       // TScanRange
#include "LtmTask.h"         // TLinkToMapTask
//---------------------------------------------------------------------------
// '������' �������� ������ � ��������
//---------------------------------------------------------------------------
class LTM_CLASS TLinkToMapScanner : public TScanRange
  {
private:
  // 2018-08-21 0 - �������� (�� ���������); > 0 - ����� (���������);
  mutable int volatile _ScanState;

  // ������ (� ����������� ������) ��� ���������� �������� �����
  TLinkToMapTask _TaskForScan;

  // ������ (� ����������� ������) ��� ��������(!) �������� �����
  TLinkToMapTask _TaskForCheck;

  unsigned int _scan_timeout_msec;

  TTimeOut _TimeOut;

  // 2018-08-14 �������� (������������) ��������������
  TMxyz _MxyzMapByKadrOut;

  bool _scan_stop_flag;

  int _scan_scale_index;
  int _check_scale_index;

  // 2018-08-18 '������� �� ������'
  TDxyn _PixRegion;

  int _LastScanIndex;
  int _FindScanIndex;

  int _PreScanResult; // 2018-09-02

#if 1
  // 2018-10-18
  struct IProgress * _progress_ptr;
public:
  struct IProgress * ProgressPtr(){return _progress_ptr;}
  void ProgressPtrSet(struct IProgress * p){_progress_ptr = p;}
private:  
#endif

  // 2019-03-19
  int _PixScanRadius;

  int _;

public:
  TLinkToMapScanner();

public:
  // 2018-10-11 ���
  TLinkToMapTask & TaskForScan(){return _TaskForScan;}
  TLinkToMapTask & TaskForCheck(){return _TaskForCheck;}

private:
  int LinkScan(int i_min, int i_max);

  // ��������� �������������� �, ���� OK, �� �������� ��� � _MxyzMapByKadrOut;
  bool MxyzMapByKadrCheck();

  void TimeOutStart(){_TimeOut.Start(TimeOutMsec());}

  bool ScanDisable(int state)const
    {
    return (_ScanState++ != 0) ?
           (_ScanState--, false) :
           ((_ScanState = state), true);
    }

  void ScanEnable()const{_ScanState = 0;}

  void Reset();

  bool ScanRangeUpdate(); // 2019-03-19 ���. �����

public:

  // �������� ������� ��������� �������
  int ScanState()const{return _ScanState;}
  int LastScanIndex()const{return _LastScanIndex;}
  int FindScanIndex()const{return _FindScanIndex;}
  int PreScanResult()const{return _PreScanResult;}

  // ����������/�������� ���� '����'
  void ScanStopFlagSet(bool on){_scan_stop_flag = on;}
  bool ScanStopFlag()const{return _scan_stop_flag;}

  // 2018-08-21
  void ScanScaleIndexSet(int index){_scan_scale_index = index;}
  int ScanScaleIndex()const{return _scan_scale_index;}

  // 2018-08-21
  void CheckScaleIndexSet(int index){_check_scale_index = index;}
  int CheckScaleIndex()const{return _check_scale_index;}

  // ����������/�������� ������� ������������ � �������������
  unsigned int TimeOutMsec()const{return _scan_timeout_msec;}
  void TimeOutMsecSet(unsigned int msec){_scan_timeout_msec = msec;}

  // 2019-03-19
  // ���� ��� ������ �������� � ������� ������������ ����������;
  int PixScanRadius()const{return _PixScanRadius;}
  void PixScanRadiusSet(int r){_PixScanRadius = r;}

  // 2019-03-19
  int PixScanRange()const{return _PixScanRadius*2;}
  void PixScanRangeSet(int r){_PixScanRadius = r/2;}

  // ������ ����� ����� ��� ������ �������� � �������� (reset);
  bool MrKadrSet(const class TMemRect & Mr);

  // 2018-10-05 scale?
  TIxy KadrSize()const{return _TaskForScan.SrcKadrSize();}

  bool PixRegionSet(const TDxy * dxy, unsigned int n);

  // ����� � ����� ����������� ���������� �������;
  // > 0 - ������� �������;
  // = 0 - ������� �� �������, ���� �����������;
  // < 0 - ������, ����������� ������;
  int PreScan();

  // 2018-08-18 '��������� ������������' (����� PreScan())
  bool RunScan();

  bool ScanExecute();

  // 2018-08-14 �������� ����������� ��������������
  bool MxyzMapByKadrGet(TMxyz & M)const;


  };
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
// 2018-08-12
TLinkToMapScanner & LTM_FUNC LtmScannerRef();

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
#endif
