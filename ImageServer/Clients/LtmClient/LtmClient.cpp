//---------------------------------------------------------------------------
// 2018-12-02
// ��� ������ ��� ������� - ������ � ���������� ������� ltm_ui_dll.dll;
// ������������ � LtmServer;
// ������ ���� �-�� extern ILtmServerInterface * LtmServerPtr()
// (����� ���� �����. NULL, �� ����� ������ �� ������ ���������� � �������)
//---------------------------------------------------------------------------
#pragma once
#pragma hdrstop
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
#include "LtmClient.h"
#include <tchar.h>
#include "LtmServerInterface.h"
#include "LtmClientInterface.h"

#include <iostream>

#include <windows.h>       // LoadLibrary � �.�.

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
static
void * DllFunc(const char * dll_name, const char * func_name)
{
    HINSTANCE lib = NULL;
    HMODULE hm = GetModuleHandleA(dll_name);


    if(!hm)
    {

        std::cout << "GetLastError: " << GetLastError() << std::endl;

        lib = LoadLibraryA(dll_name);
        if(!lib){
            std::cout << "LoadLibraryA error" << std::endl;
            return NULL;
        }

        hm = lib;
    }



    void * fun = GetProcAddress(hm, func_name);
    std::cout << "GetProcAddress: " << fun << std::endl;

    if(!fun)
    {
        if(lib)
            FreeLibrary(lib);

        return NULL;
    }

    return fun;
}
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
static ILtmClientInterface * _LtmClientPtr = 0;
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
ILtmClientInterface * LtmClientPtr()
{
    typedef ILtmClientInterface * (__stdcall *PF)(ILtmServerInterface *);
    static const char dll_name[] = "ltm_ui_dll.dll";
    static const char func_name[] = "ltm_interface_init";

    PF f = (PF)DllFunc(dll_name, func_name);
    if(!f){
        std::cout << "DllFunc error" << std::endl;
        return NULL;
    }

    //extern ILtmServerInterface * LtmServerPtr();
    ILtmServerInterface * server = 0;

    return f(server);
}
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
bool LtmClientInit()
{
    _LtmClientPtr = LtmClientPtr();
    if(!_LtmClientPtr)
        return false;

    _LtmClientPtr->init();

    //std::cout <<_LtmClientPtr->init;

    _LtmClientPtr->command(LTM_CLIENT_CMD_FLAG_FLIP_SET, 1);

    return true;
}
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
long LtmClientCommand(long cmd, long param)
{
#if 1
    // 2018-11-19 ������ ������� ����� ���� ������� �� LtmClientInit()
    if(!_LtmClientPtr)
    {
        //_LtmClientPtr = LtmClientPtr();
        LtmClientInit();
        if(!_LtmClientPtr)
            return 0;
    }

    return _LtmClientPtr->command(cmd, param);
#else
    return _LtmClientPtr ? _LtmClientPtr->command(cmd, param) : 0;
#endif
}
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
long LtmClientFormCameraShow()
{
    return LtmClientCommand(LTM_CLIENT_CMD_FORM_CAMERA, 1);
}
long LtmClientSubstrateShow()
{
    return LtmClientCommand(LTM_CLIENT_CMD_FORM_SUBSTRATE, 1);
}
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
bool LtmClientShow()
{
    LtmClientFormCameraShow();
    LtmClientSubstrateShow();
    return true;
}

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
bool LtmClientStep(long msec, const tagBITMAP * bitmap)
{
    if(!_LtmClientPtr || !bitmap)
        return false;

    _LtmClientPtr->process(msec, bitmap);

    return true;
}
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
bool LtmClientHide()
{
    if(!_LtmClientPtr)
        return false;

#if 1
    // ��������(param=1)/������(param=0) ���� '������';
    _LtmClientPtr->command(LTM_CLIENT_CMD_FORM_CAMERA, 0);

    // ��������(param=1)/������(param=0) '���� ��������';
    _LtmClientPtr->command(LTM_CLIENT_CMD_FORM_SUBSTRATE, 0);

    // ��������(param=1)/������(param=0) '���� ���������';
    _LtmClientPtr->command(LTM_CLIENT_CMD_FORM_PHOTOPLAN, 0);
#else
    _LtmClientPtr->hide();
#endif

    return true;
}
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
bool LtmClientExit()
{
    if(ILtmClientInterface * p = _LtmClientPtr)
    {
        _LtmClientPtr = NULL;
        p->exit();
        return true;
    }

    return false;
}
bool LtmClientSendCoordinates(int msec, const double * data)
{
    if (!_LtmClientPtr || !data)
        return false;

    _LtmClientPtr->send_data(msec, data, LTM_DATA_TYPE_LAT_LON_ALT, 3 * sizeof(double));

    return true;
}
//===========================================================================
//
//===========================================================================
bool LtmClientSendAngles(int msec, const double * data)
{
    if (!_LtmClientPtr || !data)
        return false;

    _LtmClientPtr->send_data(msec, data, LTM_DATA_TYPE_GAM_TET_PSI, 3 * sizeof(double));

    return true;
}
//===========================================================================
//
//===========================================================================
class TLtmClient : public ILtmClientInterface
{
private:
    ILtmClientInterface * _p;
public:
    virtual int LTMC_DECL version(){return _p ? _p->version() : 0;}
    virtual int LTMC_DECL init(){return _p ? _p->init() : 0;}
    //virtual int LTMC_DECL show(){return _p ? _p->show() : 0;}
    //virtual int LTMC_DECL open(void * hwnd){return _p ? _p->open(hwnd) : 0;}
    //virtual int LTMC_DECL hide(){return _p ? _p->hide() : 0;}
    virtual int LTMC_DECL exit(){return _p ? _p->exit() : 0;}

    virtual int LTMC_DECL process(int msec, const BITMAP * bitmap)
    {return _p ? _p->process(msec, bitmap) : 0;}

    virtual int LTMC_DECL command(int cmd, int param)
    {return _p ? _p->command(cmd, param) : 0;}

    virtual int LTMC_DECL send_data(int msec,
                                    const void * data,
                                    int data_type,
                                    unsigned int bytes)
    {return _p ? _p->send_data(msec, data, data_type, bytes) : 0;}

    virtual int LTMC_DECL msec_data_get(int msec,
                                        void * data,
                                        int data_type,
                                        unsigned int bytes)
    {return _p ? _p->msec_data_get(msec, data, data_type, bytes) : 0;}
    //-------------------------------------------------------------------------
public:
    TLtmClient() : _p(NULL)
    {
        _p = LtmClientPtr();
    }
};
//===========================================================================
//
//===========================================================================
//TLtmClient _LtmClient;
//ILtmClientInterface & LtmClientRef(){return _LtmClient;}
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
