//---------------------------------------------------------------------------
// 2018-12-02
//---------------------------------------------------------------------------
#ifndef LtmClientH
#define LtmClientH
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
#include "LtmClientInterface.h"


//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
bool LtmClientInit();
bool LtmClientShow();
bool LtmClientStep(long msec, const struct tagBITMAP * bitmap);
bool LtmClientHide();
bool LtmClientExit();
bool LtmClientSendCoordinates(int msec, const double * data);
bool LtmClientSendAngles(int msec, const double * data);
long LtmClientFormCameraShow();




//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
#endif
