//---------------------------------------------------------------------------
// 2018-04-04
// Bitmap Header Definition (�� wingdi.h);
// !!! �� �������� � ������������ ����� !!!
// !!! � cpp-������ �� �������� ����� ���������� windows.h !!!
//---------------------------------------------------------------------------
#ifndef _win_bitmap_h_
#define _win_bitmap_h_
//---------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------
#if !defined(_WINGDI_) && !defined(WINGDI_BITMAP_DEFINED)
  #define WINGDI_BITMAP_DEFINED
  #pragma pack(push, 2)
  typedef struct tagBITMAP
    {
    long            bmType;
    long            bmWidth;
    long            bmHeight;
    long            bmWidthBytes;
    unsigned short  bmPlanes;
    unsigned short  bmBitsPixel;
    void *          bmBits;
    } BITMAP;
  #pragma pack(pop)
#endif
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
#endif //_win_bitmap_h_
