//---------------------------------------------------------------------------
// 2018-11-21 ILtmClientInterface; ��������� ltm_ui_dll.dll (� ltm_dll.dll?)
//---------------------------------------------------------------------------
#ifndef LtmClientInterfaceH
#define LtmClientInterfaceH
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
#define LTM_CLIENT_VERSION 1
//---------------------------------------------------------------------------
// Bitmap Header Definition (�� wingdi.h); 
//---------------------------------------------------------------------------
#if 0
  #define WINGDI_BITMAP_DEFINED
  #pragma pack(push, 2)
  typedef struct tagBITMAP
    {
    long            bmType;
    long            bmWidth;
    long            bmHeight;
    long            bmWidthBytes;
    unsigned short  bmPlanes;
    unsigned short  bmBitsPixel;
    void *          bmBits;
    } BITMAP;
  #pragma pack(pop)
#else
  struct tagBITMAP;  
#endif
//===========================================================================
// ���� ������������ ������ (��� ������ ILtmClientInterface::send_data())
//-------------------------------------------------------------
// LTM_DATA_TYPE_LAT_LON_ALT:
// ���������� ����, ���������� �� GPS-������� (������ �������� �������);
// double[0] - ������ � ��������;
// double[1] - ������� � ��������;
// double[2] - ������ � ������;
//-------------------------------------------------------------
// LTM_DATA_TYPE_GAM_TET_PSI:
// ���� ���� (������ �������� �������);
// double[0] - ���� � ��������;
// double[1] - ������ � ��������;
// double[2] - ���� � ��������;
//===========================================================================
// ����� (LTM_DATA_TYPE_CAMERA_...) ������,
// ������� ���� ���������� �� ������� �������;
// ���� �������� - ��� ��� ������ ...
// ���� ������������ ILtmClientInterface::msec_data_get();
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ���� (data != NULL), �� ����������� ������
// ��������������� ��� ���������������� �� ���� ��������� ��������;
// ���� (data == NULL), �� msec_data_get() ���������� ��������� � msec
// ����� ������ ��� ������ msec ��� �������� ���� ����������� ������;
// msec_data_get() ���������� < 0 � ������ ������ ���
// ��������� � ������������ ����� (����) � ������ ������;
//-------------------------------------------------------------
// LTM_DATA_TYPE_CAMERA_LAT_LON_ALT:
// ���������� ����, ����������� � fly_dll;
// double[0] - ������ � ��������;
// double[1] - ������� � ��������;
// double[2] - ������ � ������;
//-------------------------------------------------------------
// LTM_DATA_TYPE_CAMERA_GAM_TET_PSI:
// ���� ����, ����������� � ltm_dll;
// double[0,1,2] - (����, ������, ����) � ��������;
//-------------------------------------------------------------
// LTM_DATA_TYPE_USER_MXYZ_MAP_BY_KADR:
// ��������/���������� ��������� ����������� �������������� � ����� �� ��������
// ��� ������ ���� ��� ������ ...
// ��� �������������� ����� ���������� � ������������� � ���� ��������;
//-------------------------------------------------------------
// LTM_DATA_TYPE_MSEC_MXYZ_MAP_BY_KADR:
// ��������/���������� ����������� �������������� � ����� �� ��������
// ��� �����[msec];
//-------------------------------------------------------------
// 2019-03-25
// LTM_DATA_TYPE_PYR_FILE_NAME:
// �������� ��� ����� �������� ������� ��������
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// int id = LTM_DATA_TYPE_PYR_FILE_NAME;
// // �������� ����� ������ (��� �������������� 0)
// int n = msec_data_get(0, NULL, id, 0);
// // char s[n+1];
// // ��������� ������; ���������� �����. ����������� ��������;
// int n1 = msec_data_get(0, s, id, n+1);
//===========================================================================
#ifndef LTM_DATA_TYPES_DEFINED
  #define LTM_DATA_TYPES_DEFINED

  #define LTM_DATA_TYPE_LAT_LON_ALT 1L
  #define LTM_DATA_TYPE_GAM_TET_PSI 2L

  #define LTM_DATA_TYPE_IMG_FILE_NAME 11L
  //#define LTM_DATA_TYPE_IMG_SCALE 12L

  #define LTM_DATA_TYPE_USER_MXYZ_MAP_BY_KADR 14L

  #define LTM_DATA_TYPE_MSEC_MXYZ_MAP_BY_KADR 15L

  #define LTM_DATA_TYPE_PYR_FILE_NAME 20L

#endif
//===========================================================================
// ���� �������� �����(������) � ��������
// ���� �� ������ �����, �� ������� ����� ���������� 4-� ����� �����;
// FRAME_LINK_TYPE_MISSING - �������� ����������� (�� ������); (������ = 0)!
// FRAME_LINK_TYPE_PIXELS  - ���������� ���������� �� ��������;
// FRAME_LINK_TYPE_METERS  - ���-���������� (����� �� ���������);
// FRAME_LINK_TYPE_RADIANS - ���-���������� (�������: ������, �������);
//===========================================================================
#ifndef FRAME_LINK_TYPE_DEFINED
#define FRAME_LINK_TYPE_DEFINED
  #define FRAME_LINK_TYPE_MISSING 0L
  #define FRAME_LINK_TYPE_PIXELS  1L
  #define FRAME_LINK_TYPE_METERS  2L
  #define FRAME_LINK_TYPE_RADIANS 3L
  #define FRAME_LINK_TYPE_ERROR (-1L)
#endif
//===========================================================================
// �������������� ������ ��� ������
// ILtmClientInterface::command(int cmd, int param)
//===========================================================================
// ��������(param=1)/���������(param=0) ��������������� ��������
// (���������� ����������� '����-���');
#define LTM_CLIENT_CMD_FLAG_FLIP_SET 101

// �������� ������� ����� (����);
#define LTM_CLIENT_CMD_CURR_MSEC_GET 102

// ��������(param=1)/������(param=0) ���� '������';
// ���� param �� ����� 0 ��� 1, �� �� ����� ���. ��� ��������� �� TComponent;
#define LTM_CLIENT_CMD_FORM_CAMERA 103

// ��������(param=1)/������(param=0) ���� '������';
#define LTM_CLIENT_CMD_FORM_TIMER_TREE 105

// ��������(param=1)/������(param=0) ���� '��������';
// ���� param �� ����� 0 ��� 1, �� �� ����� ���. ��� ��������� �� TComponent;
#define LTM_CLIENT_CMD_FORM_SUBSTRATE 109

// ��������(param=1)/������(param=0) ���� '����';
// ���� param �� ����� 0 ��� 1, �� �� ����� ���. ��� ��������� �� TComponent;
#define LTM_CLIENT_CMD_FORM_PHOTOPLAN 209

// ��������(param=1)/������(param=0) ���� '�����';
// ���� param �� ����� 0 ��� 1, �� �� ����� ���. ��� ��������� �� TComponent;
#define LTM_CLIENT_CMD_FORM_LTM_TESTS 210

// �������� � param = handle �������� ���� ����������;
#define LTM_CLIENT_CMD_APP_HANDLE_SET 110
#define LTM_CLIENT_CMD_APP_PTR_SET  111
#define LTM_CLIENT_CMD_RESET        112
#define LTM_CLIENT_CMD_FORMS_UPDATE 113

// ��������(param=1)/������(param=0) ���� '������� ������'
// ���� param �� ����� 0 ��� 1, �� �� ����� ���. ��� ��������� �� TComponent;
#define LTM_CLIENT_CMD_FORM_IMS_FRAMES 121

// ��������(param=1)/������(param=0) ���� '����������� ������'
#define LTM_CLIENT_CMD_FORM_VIRTUAL_CAMERA 122

// ��������/������ ���� '����������� ������ ��� �������� �������'
#define LTM_CLIENT_CMD_FORM_FOR_TRAN_CALC 123

// ��������/������ ���� '����������� ������ ��� �������� � ��������'
#define LTM_CLIENT_CMD_FORM_LINK_TO_MAP 124

// ������� ������ '���������� �������� �������'
// ���� param �� ����� 0 ��� 1, �� �� ����� ���. ��� ��������� �� TComponent;
#define LTM_CLIENT_CMD_GEO_TILES_TOOLS 150

//
#define LTM_CLIENT_CMD_LAST_TRAN_ERROR 205

// ��������/���������/�������� ���� '��������� ���������';
// TLtmClientInterface::command(cmd, param);
// cmd = LTM_CLIENT_CMD_KEY_TRAN_CALC_FLAG;
// param = 0 - ���������, 1 - ��������, 2 - ��������;
#define LTM_CLIENT_CMD_KEY_TRAN_CALC_FLAG 123

// 2018-11-21 ����� ���� �� ����� ...
#define LTM_CLIENT_CMD_OPTIONS_LOAD_BY_SIZE 127

// ��������(param=1)/������(param=0) ���� 'LTM-client';
// ���� param �� ����� 0 ��� 1, �� �� ����� ���. ��� ��������� �� TComponent;
#define LTM_CLIENT_CMD_FORM_LTM_CLIENT 333

// ��������(��������) ������� ����� ����� ������� (param = msec);
#define LTM_CLIENT_CMD_MSEC_PER_FRAME_SET 145

// ��������� ������ '��������� �������� � �������� (LTM)'
#define LTM_CLIENT_CMD_LTM_OPTIONS_DIALOG 146

// ��������/������ ���� '����������� ������ ��� �������� ��������'
#define LTM_CLIENT_CMD_FORM_LINK_CHECK 147

// ��������(��������) ������� ����� (�����������!) ����� ������� (param = nsec);
#define LTM_CLIENT_CMD_NSEC_PER_FRAME_SET 148

// ��������� ������ �������� �������� ������ (CAMERA.XML)
#define LTM_CLIENT_CMD_PNB_CAMERA_DIALOG 149

// ���������� �������� <�������� ��������� �� param ��������>;
// ���� �������������� ������ ���� 0 � 180;
// ������������ ����, ����� ��������� ���������� ���� �� �������� � �.�.;
// ����� - �������� �� ���� - ����� ��������� ������������ ��������� ������ ...
#define LTM_CLIENT_CMD_ROTATED_DEGREES_SET 180

// ��������� ������ �������� �������
#define LTM_CLIENT_CMD_ZMR_LOAD_DIALOG 181

// 2019-03-19 ������� ��� ����������; ���������� 0 ��� 1;
#define LTM_CLIENT_CMD_FORM_CAMERA_VISIBLE 182

// ������� ��� ����������; ���������� 0 ��� 1;
#define LTM_CLIENT_CMD_FORM_SUBSTRATE_VISIBLE 183

// ������� ��� ����������; ���������� 0 ��� 1;
#define LTM_CLIENT_CMD_FORM_PHOTOPLAN_VISIBLE 184

// ������� ��� ����������; ���������� 0 ��� 1;
#define LTM_CLIENT_CMD_FORM_LTM_TESTS_VISIBLE 185

#define LTM_CLIENT_CMD_FORM_SUBSTRATE_UPDATE 191
#define LTM_CLIENT_CMD_PYRAMID_LOAD_DIALOG 192
#define LTM_CLIENT_CMD_PYRAMID_LOAD_FROM_FILE 193

#define LTM_CLIENT_CMD_FRAME_REGION_TRY_LOAD_DIALOG 195

// 2018-12-15 �������� ��������� �� ������� void __cdecl f(),
// ������� ���� �������� ��� ��������� �������� (options)
//typedef void __cdecl (*PF_VOID_CDECL)();
#define LTM_CLIENT_CMD_OPTIONS_CHANGED_FUNC 196

// ���������� ���. FLY-��������� � PNB-��������� �� LTM-����������,
// �.�. ������� �-��, �������� �������� LTM_CLIENT_CMD_OPTIONS_CHANGED_FUNC;
#define LTM_CLIENT_CMD_OPTIONS_CHANGED_CALL 197

// 2019-02-07 ��������� ������ '��������� IMS-�������'
#define LTM_CLIENT_CMD_IMS_OPTIONS_DIALOG 198

// 2019-02-11 ��������� ������ '��������� �������� ������' (�������� �����)
#define LTM_CLIENT_CMD_DIC_OPTIONS_DIALOG 199

// 2019-02-15 ������� ��������� � ������������� �����
#define LTM_CLIENT_CMD_TRAN_SCAN_TEST 201

//===========================================================================
// 2019-03-16 comments[mxyz]:
// M - ������� 3x3 ������������ �������������� � ������ �� �����;
// ������ ��������� �� ��c��� � ����� ������� ����;
// (x,y) - ���������� �� src-������;
// (x',y',z') - ����������� ���������� �� dst-������;
// (x'/z', y'/z') - ���������� �� dst-������;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// x' = M[0]*x + M[1]*y + M[2];
// y' = M[3]*x + M[4]*y + M[5];
// z' = M[6]*x + M[7]*y + M[8];
//===========================================================================
//---------------------------------------------------------------------------
// LTMC - Link To Map Client
//---------------------------------------------------------------------------
#ifndef LTMC_DECL
  #ifdef __gnu_linux__
    #define LTMC_DECL
  #else
    #define LTMC_DECL __stdcall
  #endif
#endif
//---------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------
struct ILtmClientInterface
  {
public:

  // ����� ������ ����� ����������;
  virtual int LTMC_DECL version() = 0;

  // ������� ����� �������� DLL (���������������� ������);
  virtual int LTMC_DECL init() = 0;

  // ������� ����� ��������� ����������-�������;
  virtual int LTMC_DECL exit() = 0;

  // ��������� ������� (��. LTM_CLIENT_CMD_...)
  virtual int LTMC_DECL command(int cmd, int param) = 0;

  // ���������� ����� ����
  virtual int LTMC_DECL process(int msec,
                                const tagBITMAP * bitmap) = 0;

  // �������� ������ (����. ����������) ��� �����[msec]
  // data_type = LTM_DATA_TYPE_...
  virtual int LTMC_DECL send_data(int msec,
                                  const void * data,
                                  int data_type,
                                  unsigned int bytes) = 0;

  // �������� (���������) ������ (����. ���������� � ����) ��� ������� msec;
  // data_type = LTM_DATA_TYPE_...
  // � ������ ������ ���������� ���������� ����������� ����(!);
  // ���� �� ���� - �� ����� �� ���� �� ����� ����� � ���������� ������� -
  // ���-�� ���� ILtmServerInterface::send_pack(const void * data);
  virtual int LTMC_DECL msec_data_get(int msec,
                                      void * data,
                                      int data_type,
                                      unsigned int bytes) = 0;

  // �������� ����� ����� ��� ������� msec;
  // �����. <=0 � ������ ������ (��������, ���� ��� �����[msec]);
  // ����� � bm==NULL ��������� rect[] � �����. �����. ����(!) �� �������;
  // ��� ������ ��� ��������� ���������� ������ (bm != NULL)
  // 1) bm->bmType = 0; bm->bmPlanes = 1; (������ ��� �������� ������������)
  // 2) bm->bmBits ������ ��������� �� ���������� ������
  // 3) bm->bmBitsPixel ����� ���������� = {8,24,32};
  // 4) bm->bmWidthBytes ������ ���� ������ 2 (WINAPI);
  // ����� ��������� �� ���� ����� (rect != NULL), �� �����
  // ������� �������������� ������ ���� ����� �������� �������;
  virtual int LTMC_DECL msec_bitmap_get(int msec,
                                        int rect[4],
                                        const tagBITMAP * bitmap) = 0;

  // 2019-03-15
  virtual int LTMC_DECL mxyz_dst_by_src_calc(double M[3*3],
                                             const tagBITMAP * dst_bitmap,
                                             const tagBITMAP * src_bitmap) = 0;

  virtual int LTMC_DECL frame_to_frame_mxyz_get(int src_msec,
                                                int dst_msec,
                                                double m[3*3],
                                                const int size[2]) = 0;

  // �������� �������������� (����� � �������������) �������� ������;
  // ����������:
  // ���� (msec == NULL), �� ���������� �������� ������;
  // ���� (msec != NULL), �� ���������� ���������� msec[];
  // ������ �������� ������ �������� ����� ��������� ����� ������;
  virtual int LTMC_DECL key_frames_msec_get(int * msec, int n) = 0;

  // ��������� ����� bitmap ���������� ������ �������� �� ����������������;
  // deg[2*4] - ���������� (������ � ������� � ��������) �� ���������
  // ����� ����������������, ��������������� ����� ������������ ������;
  // (deg[0], deg[1]) - �����  ������� ���� ������������ ������;
  // (deg[2], deg[3]) - ������ ������� -//-;
  // (deg[4], deg[5]) - ������ ������  -//-;
  // (deg[6], deg[7]) - �����  ������  -//-;
  virtual int LTMC_DECL map_read_block(const tagBITMAP * bitmap,
                                       const double * deg,
                                       int flags) = 0;

  // �������� ��������� �� ��������� '������� �������� ������ � ��������';
  // (��. LtmScanInterface.h)
  virtual struct ILtmScanInterface * LTMC_DECL ltm_scanner() = 0;

  // �������(!) '�������� �������� ������ � ��������';
  // (��. LtmContextInterface.h)
  virtual struct ILtmContext * LTMC_DECL create_ltm_context() = 0;
  };
//---------------------------------------------------------------------------
// ����� ����� �� �������� "LtmServerInterface.h"
//---------------------------------------------------------------------------
struct ILtmServerInterface;
//---------------------------------------------------------------------------
// ������� ������ ������������ ������� � �������
//---------------------------------------------------------------------------
extern "C"
#ifdef __GNUC__
__attribute__((visibility("default")))
#else
__declspec(dllexport)
#endif

// 2018-12-09 �������� �������� ������� ltm_
ILtmClientInterface * LTMC_DECL ltm_interface_init(ILtmServerInterface * intfs);
//---------------------------------------------------------------------------
// ��� ��� ltm_dll, ��(!) �������������� �������;
//---------------------------------------------------------------------------
ILtmClientInterface & LtmClientRef();
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------


#endif
