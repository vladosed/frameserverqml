#include "ltmmodule.h"

LtmModule::LtmModule(QObject *parent) : QObject(parent)
{
    assert((!LtmClientInit()));
    //loadTelemetry(QString("D:/17_11_09__14_38_57_Repair.tlm"));
}

void LtmModule::showWindow()
{
    LtmClientShow();
    qDebug() << "LtmClientShow()";
}

double radian_by_gradus(double g)
{
    double rad = g * (M_PI / 180.0);
    return rad;
}

void LtmModule::nextStep(int millis, const cv::Mat &frame)
{
    this->m_bitmap = new tagBITMAP();

    int width = frame.cols;
    int height = frame.rows;

    char * bits = new char[width * height * 3];

    int p = 0;
    // for (int i = height - 1; i >= 0; i--) {
    for (int i = height; i-- > 0;) {

        for (int j = 0; j < width; j++) {\
            auto pixel = frame.at<cv::Vec3b>(cv::Point(j, i));
            bits[p + 0] = pixel.val[0];
            bits[p + 1] = pixel.val[1];
            bits[p + 2] = pixel.val[2];
            p += 3;
        }
    }

    m_bitmap->bmType = 0;
    m_bitmap->bmWidth = width;
    m_bitmap->bmHeight = height;
    m_bitmap->bmWidthBytes = 3 * width; // должно быть кратно двум (читай WinBitmap)
    m_bitmap->bmBitsPixel = 24;
    m_bitmap->bmBits = bits;

    //std::cout << "LtmClientStep(1, bitmap)" << endl;
    LtmClientStep(millis, m_bitmap);

    {
        double lat = telemPoints.at(millis).getLat();
        double lon = telemPoints.at(millis).getLon();
        double alt = telemPoints.at(millis).getAlt();
        double data[3] = {radian_by_gradus(lat), radian_by_gradus(lon), alt};
        LtmClientSendCoordinates(millis, data);
    }
    {
        double gam = telemPoints.at(millis).getGam();
        double tet = telemPoints.at(millis).getTet();
        double psi = telemPoints.at(millis).getPsi();
        double data[3] = {radian_by_gradus(gam), radian_by_gradus(tet), radian_by_gradus(psi)};
        LtmClientSendAngles(millis, data);
    }


    delete[] bits;
}

void LtmModule::loadTelemetry(QString filename)
{
    QFile inputFile(filename);
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList list = line.split("\t", QString::SkipEmptyParts);
            if (line[0] == "\t")
            {
                telemPoints.push_back(TelemPoint(list));
                telemPoints.last().debug();
            }

        }
        inputFile.close();
    }
}

TelemPoint::TelemPoint(double msec, double lat, double lon, double alt, double gam, double tet, double psi):
    m_msec(msec), m_lat(lat),m_lon(lon), m_alt(alt), m_gam(gam), m_tet(tet), m_psi(psi)
{

}

TelemPoint::TelemPoint(QStringList list):
    m_msec(list[0].toDouble()),
    m_lat(list[1].toDouble()),
    m_lon(list[2].toDouble()),
    m_alt(list[3].toDouble()),
    m_gam(list[4].toDouble()),
    m_tet(list[5].toDouble()),
    m_psi(list[6].toDouble())
{

}

void TelemPoint::debug()
{
    qDebug() << "msec" << m_msec <<
                "m_lat" << m_lat <<
                "m_lon" << m_lon <<
                "m_alt" << m_alt <<
                "m_gam" << m_gam <<
                "m_tet" << m_tet <<
                "m_psi" << m_psi;
}
