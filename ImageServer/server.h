#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QDebug>

#include "videostreamer.h"

class Server : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant bekProgressStatus READ getBekProgress NOTIFY bekProgressChanged)
public:
    explicit Server(QObject *parent = nullptr);

    QVariant getBekProgress(){
        return QVariant::fromValue(this->s_bekprogress);
    }
    Q_INVOKABLE void startVideoStreaming();
    Q_INVOKABLE void startBekPostProc(QString map, QString listImages);
    Q_INVOKABLE void setTelemetryTLM(QString filename);
signals:
    void bekProgressChanged();

public slots:
    void bekProgress(int max, int current);

private:
    VideoStreamer streamer;

    QVariant s_bekprogress = "";
};

#endif // SERVER_H
