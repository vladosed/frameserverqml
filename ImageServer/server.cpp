#include "server.h"

Server::Server(QObject *parent) : QObject(parent)
{
    QObject::connect(&streamer, &VideoStreamer::BekProgress, this, &Server::bekProgress);
}


void Server::startVideoStreaming()
{
    streamer.startStream();
}

void Server::startBekPostProc(QString map, QString listImages)
{
    map = map.replace("file:///", "");
    listImages = listImages.replace("file:///", "");
    streamer.startBek(map, listImages.split(","));
}

void Server::setTelemetryTLM(QString filename)
{
    filename = filename.replace("file:///", "");
    filename = QString("D:/17_11_09__14_38_57_Repair.tlm");
    QFile inputFile(filename);
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       while (!in.atEnd())
       {
          QString line = in.readLine();
          qDebug() << line;
       }
       inputFile.close();
    }

}

void Server::bekProgress(int max, int current)
{
    QString progress = QString("%1 из %2").arg(current).arg(max);
    this->s_bekprogress = progress;
    emit bekProgressChanged();

}


