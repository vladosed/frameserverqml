#include "videostreamer.h"

VideoStreamer::VideoStreamer(QObject *parent) : QObject(parent)
{
    this->m_cap.open("D:/sky/17_11_09__14_38_57_Repair.avi");
    if(!this->m_cap.isOpened())
    {
        qDebug() << "Camera capture error";
    }
}

VideoStreamer::~VideoStreamer()
{
    this->videoCapturing = false;
}

void VideoStreamer::startStream()
{
    ltmModule.showWindow();
    this->videoCapturing = true;
    QFuture<void> future = QtConcurrent::run(this, &VideoStreamer::capture);
}

void VideoStreamer::stopStream()
{
    this->videoCapturing = false;
}

void VideoStreamer::capture()
{
    int time = 0;
    while (this->videoCapturing)
    {
        cv::Mat smallFrame;
        this->m_cap >> frame;

        if (frame.empty()){
            qDebug() << "frame is empty";
            return;
        }
        this->broadcast(time);
        time++;
    }
}

void VideoStreamer::broadcast(int msec)
{
    ltmModule.nextStep(msec, this->frame);
}

void VideoStreamer::startBek(QString map, QStringList listImages)
{
    this->map_name = map;
    this->photo_list = listImages;

    QFuture<void> future = QtConcurrent::run(this, &VideoStreamer::bekPostProc);

    qDebug() << map;
    qDebug() << listImages;
}

void VideoStreamer::bekPostProc()
{
    int i = 0;
    for (auto img : this->photo_list){

        emit BekProgress(this->photo_list.count(), i);
        qDebug() << this->map_name << img;
        convModule.compareTwoImages(this->map_name, img);

        i++;
        emit BekProgress(this->photo_list.count(), i);
    }
}
