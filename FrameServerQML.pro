QT += quick network
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    Stitch/Stitch.cpp \
    Stitch/inputdata.cpp \
    Stitch/stitchthread.cpp \
    Link/communicator.cpp \
    ImageServer/Clients/LtmClient/LtmClient.cpp \
    ImageServer/Clients/ltmmodule.cpp \
    ImageServer/server.cpp \
    ImageServer/Clients/convolutionmodule.cpp \
    ImageServer/videostreamer.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
INCLUDEPATH += D:/opencv-master/BUILD/install/include

LIBS += -LD:/Qt/workspace/find_pos_qt -lfly_pnb_dll

Release: LIBS += -L"D:/opencv-master/BUILD/install/x86/vc15/lib" \
            -lopencv_core400 \
            -lopencv_highgui400 \
            -lopencv_imgproc400 \
            -lopencv_features2d400 \
            -lopencv_xfeatures2d400 \
            -lopencv_flann400 \
            -lopencv_imgcodecs400 \
            -lopencv_stitching400\
            -lopencv_video400 \
            -lopencv_videoio400 \
            -lopencv_calib3d400 \

Debug: LIBS += -L"D:/opencv-master/BUILD/install/x86/vc15/lib" \
            -lopencv_core400 \
            -lopencv_highgui400 \
            -lopencv_imgproc400\
            -lopencv_features2d400 \
            -lopencv_xfeatures2d400 \
            -lopencv_flann400 \
            -lopencv_imgcodecs400 \
            -lopencv_stitching400\
            -lopencv_video400 \
            -lopencv_videoio400 \
            -lopencv_calib3d400 \



#LIBS += E:/opencv/build/install/x86/mingw/bin/libopencv_core345.dll
#LIBS += E:/opencv/build/install/x86/mingw/bin/libopencv_highgui345.dll
#LIBS += E:/opencv/build/install/x86/mingw/bin/libopencv_imgcodecs345.dll
#LIBS += E:/opencv/build/install/x86/mingw/bin/libopencv_imgproc345.dll
#LIBS += E:/opencv/build/install/x86/mingw/bin/libopencv_features2d345.dll
#LIBS += E:/opencv/build/install/x86/mingw/bin/libopencv_xfeatures2d345.dll
#LIBS += E:/opencv/build/install/x86/mingw/bin/libopencv_calib3d345.dll

HEADERS += \
    Stitch/Stitch.h \
    Stitch/inputdata.h \
    Stitch/stitchthread.h \
    Link/communicator.h \
    mavlink/opt_sensor/gtestsuite.hpp \
    mavlink/opt_sensor/mavlink.h \
    mavlink/opt_sensor/mavlink_msg_set_algorithm_status.h \
    mavlink/opt_sensor/mavlink_msg_set_algorithm_status.hpp \
    mavlink/opt_sensor/mavlink_msg_set_video_status.h \
    mavlink/opt_sensor/mavlink_msg_set_video_status.hpp \
    mavlink/opt_sensor/mavlink_msg_test_types.h \
    mavlink/opt_sensor/mavlink_msg_test_types.hpp \
    mavlink/opt_sensor/opt_sensor.h \
    mavlink/opt_sensor/opt_sensor.hpp \
    mavlink/opt_sensor/testsuite.h \
    mavlink/opt_sensor/version.h \
    mavlink/checksum.h \
    mavlink/mavlink_conversions.h \
    mavlink/mavlink_get_info.h \
    mavlink/mavlink_helpers.h \
    mavlink/mavlink_sha256.h \
    mavlink/mavlink_types.h \
    mavlink/message.hpp \
    mavlink/msgmap.hpp \
    mavlink/protocol.h \
    ImageServer/Clients/LtmClient/LtmClient.h \
    ImageServer/Clients/LtmClient/LtmClientInterface.h \
    ImageServer/Clients/LtmClient/LtmServerInterface.h \
    ImageServer/Clients/LtmClient/win_bitmap.h \
    ImageServer/Clients/ltmmodule.h \
    ImageServer/server.h \
    ImageServer/Clients/ConvolutionClient/find_pos.h \
    ImageServer/Clients/ConvolutionClient/OWN.H \
    ImageServer/Clients/ConvolutionClient/PNB_DBG.H \
    ImageServer/Clients/ConvolutionClient/TG_RASTR.H \
    ImageServer/Clients/convolutionmodule.h \
    ImageServer/videostreamer.h

DISTFILES += \
    Viewer/shaders/texture.vert \
    Viewer/shaders/texture.frag
