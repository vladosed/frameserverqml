import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.12

Item {

    ColumnLayout{
        spacing: 2
        anchors.fill: parent

        Rectangle {
            id: startVideo

            Layout.preferredWidth: parent.width
            Layout.preferredHeight: parent.height * 0.2
            border.width: 2
            border.color: "black"
            Text{
                text: "Входные данные"
                anchors.top: parent.top
                font.pointSize: 14
                anchors.topMargin: 20
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Row{
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                spacing: 10

                Button{
                    text: "Выбрать видео"
                    onClicked: {
                    }
                }
                Button{
                    text: "Камера"
                    onClicked: {
                    }
                }
            }
        }

        Rectangle {
            id: seqBox
            Layout.preferredWidth: parent.width
            //Layout.preferredHeight:
            Layout.preferredHeight: parent.height * 0.3
            color: "transparent"
            border.width: 2
            border.color: "black"

            SequencePage{
                anchors.fill: parent
            }

        }

        Rectangle {
            Layout.preferredWidth: parent.width
            Layout.fillHeight: true
            color: "transparent"
            border.width: 2
            border.color: "black"
            GeoreferencePage{
                anchors.fill: parent
            }
        }

        Rectangle {

            Layout.preferredWidth: parent.width
            Layout.preferredHeight: parent.height * 0.2
            color: "transparent"
            Column{
                anchors.fill: parent
                Text{
                    anchors.horizontalCenter: parent.horizontalCenter
                    text:"Управление видео"
                    font.pointSize: 14
                }

                Row{
                    anchors.horizontalCenter: parent.horizontalCenter
                    Button{
                        id: playVideo
                        text: "Старт"
                        enabled: false
                        onClicked: {
                        }
                    }
                }
            }
        }
    }

}
